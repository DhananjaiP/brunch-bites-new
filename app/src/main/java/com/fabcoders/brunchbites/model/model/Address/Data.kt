package com.fabcoders.brunchbites.Model.Address

data class Data(
    val address1: String,
    val addtype: String,
    val aid: String,
    val city: String,
    val fulladdress: String,
    val landmark: String,
    val lat: String,
    val locality: String,
    val lon: String,
    val pincode: String
)