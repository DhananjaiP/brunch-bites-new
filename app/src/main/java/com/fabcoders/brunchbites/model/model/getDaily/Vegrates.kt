package com.fabcoders.brunchbites.model.model.getDaily

data class Vegrates(
    val productid: String,
    val rate: String
)