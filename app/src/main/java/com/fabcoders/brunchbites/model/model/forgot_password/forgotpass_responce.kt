package com.fabcoders.brunchbites.model.model.forgot_password

data class forgotpass_responce(
    val message: String,
    val status: String
)