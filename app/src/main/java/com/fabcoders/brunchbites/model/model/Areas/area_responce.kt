package com.fabcoders.brunchbites.Model.Areas

data class area_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)