package com.fabcoders.brunchbites.model.model.cart

data class OrderDetail_responce(
    val `data`: List<DataXX>,
    val discountamount: String,
    val discountpercent: String,
    val gstamount: String,
    val message: String,
    val schoolid: String,
    val status: String,
    val subtotal: String,
    val total: String,
    val vouchercode: String
)