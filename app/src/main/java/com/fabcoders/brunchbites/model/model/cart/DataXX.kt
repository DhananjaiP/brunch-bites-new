package com.fabcoders.brunchbites.model.model.cart

data class DataXX(
    val duration: String,
    val gstamount: String,
    val lineid: String,
    val subcategory: String,
    val subcategoryname: String,
    val subtotal: String,
    val total: String
)