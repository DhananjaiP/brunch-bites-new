package com.fabcoders.brunchbites.Model.Regions

data class Data(
    val regionid: String,
    val regionname: String
)