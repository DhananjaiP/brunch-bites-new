package com.fabcoders.brunchbites.Model.Categories

data class sub_category(
    val `data`: List<DataX>,
    val message: String,
    val status: String
)