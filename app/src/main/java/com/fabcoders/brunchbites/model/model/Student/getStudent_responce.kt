package com.fabcoders.brunchbites.model.model.Student

data class getStudent_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)