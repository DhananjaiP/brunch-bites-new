package com.fabcoders.brunchbites.Model.Slider

import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "offers")
data class Data(
    val sliderid: String,
    val sliderimage: String
): Serializable