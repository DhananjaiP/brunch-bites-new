package com.fabcoders.brunchbites.model.model.cart

data class cart_ItemDetails(
    val dates: List<Date>,
    val extraitems: List<Extraitem>,
    val message: String,
    val status: String
)