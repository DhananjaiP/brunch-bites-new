package com.fabcoders.brunchbites.model.model.cart

data class orderList_Responce(
    val `data`: List<DataX>,
    val message: String,
    val status: String
)