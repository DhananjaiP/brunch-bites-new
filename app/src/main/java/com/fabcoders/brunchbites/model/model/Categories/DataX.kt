package com.fabcoders.brunchbites.Model.Categories

data class DataX(
    val subcategoryid: String,
    val subcategoryname: String,
    val subtitle: String
)