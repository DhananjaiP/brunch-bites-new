package com.fabcoders.brunchbites.model.model.cart

data class add_to_cart_cal_responce(
    val lineid: Int,
    val message: String,
    val status: String
)