package com.fabcoders.brunchbites.model.model.getDaily

data class Data(
    val duration: String,
    val nonvegrates: Nonvegrates,
    val vegrates: Vegrates
)