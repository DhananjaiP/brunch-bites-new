package com.fabcoders.brunchbites.model.model.cart

data class DataX(
    val discountamount: String,
    val entrydatetime: String,
    val gstamount: String,
    val orderid: String,
    val subtotal: String,
    val total: String
)