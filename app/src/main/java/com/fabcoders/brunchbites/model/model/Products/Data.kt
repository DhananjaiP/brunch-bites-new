package com.fabcoders.brunchbites.model.model.Products

data class Data(
    val duration: String,
    val productid: String,
    val productname: String,
    val producttype: String,
    val rate: String,
    val subcat: String,
    val vegnonveg: String
)