package com.fabcoders.brunchbites.Model.Holiday

data class holiday_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)