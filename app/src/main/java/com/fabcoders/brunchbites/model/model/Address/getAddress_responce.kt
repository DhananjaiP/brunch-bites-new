package com.fabcoders.brunchbites.Model.Address

data class getAddress_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)