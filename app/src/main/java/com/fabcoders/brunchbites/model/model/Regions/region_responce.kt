package com.fabcoders.brunchbites.Model.Regions

data class region_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)