package com.fabcoders.brunchbites.Model.calendar

import java.io.Serializable

data class Data(
    val date: String,
    var veg_nonVeg: Boolean
):Serializable