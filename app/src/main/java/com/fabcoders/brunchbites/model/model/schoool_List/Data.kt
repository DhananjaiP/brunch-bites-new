package com.fabcoders.brunchbites.Model.schoool_List

data class Data(
    val address: String,
    val areaid: String,
    val areaname: String,
    val regionid: String,
    val regionname: String,
    val sbid: String,
    val sbname: String,
    val sbtype: String
//    var selected: Boolean = false
)