package com.fabcoders.brunchbites.model.model.Products

data class products_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)