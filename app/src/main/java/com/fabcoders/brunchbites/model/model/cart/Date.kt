package com.fabcoders.brunchbites.model.model.cart

data class Date(
    val deldate: String,
    val productid: String,
    val rate: String,
    val vegnonveg: String
)