package com.fabcoders.brunchbites.model.model.Student

data class add_edit_StudentResponce(
    val message: String,
    val status: String,
    val studentid: Int
)