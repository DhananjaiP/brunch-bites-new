package com.fabcoders.brunchbites.Model.Areas

data class Data(
    val areaid: String,
    val areaname: String
)