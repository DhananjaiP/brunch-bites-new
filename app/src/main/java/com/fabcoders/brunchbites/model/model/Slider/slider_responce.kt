package com.fabcoders.brunchbites.Model.Slider

data class slider_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)