package com.fabcoders.brunchbites.Model.login

data class login_responce(
    val email: String,
    val fullname: String,
    val isotpverified: String,
    val mainentity: String,
    val mainregion: String,
    val message: String,
    val status: String,
    val userid: String,
    val usertype: String,
    val mobile: String
)