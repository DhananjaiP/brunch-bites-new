package com.fabcoders.brunchbites.model.model.Address;

public class Add_Edit_address_Request {
    public String fulladdress;
    public String address1;
    public String locality;
    public String landmark;
    public String city;
    public String pincode;
    public String lat;
    public String lon;
    public String userid;
    public String addtype;
}
