package com.fabcoders.brunchbites.model.model.Address

data class add_edit_address_responce(
    val addid: Int,
    val message: String,
    val status: String
)