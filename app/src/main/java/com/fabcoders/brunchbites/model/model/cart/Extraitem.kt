package com.fabcoders.brunchbites.model.model.cart

data class Extraitem(
    val productid: String,
    val productname: String,
    val rate: String
)