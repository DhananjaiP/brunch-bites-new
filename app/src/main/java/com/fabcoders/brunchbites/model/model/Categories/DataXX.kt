package com.fabcoders.brunchbites.model.model.Categories

data class DataXX(
    val categoryid: String,
    val categoryname: String,
    val cattype: String,
    val imglink: String
)