package com.fabcoders.brunchbites.model.model.Student

data class Data(
    val classdetail: String,
    val studentid: String,
    val studentname: String
)