package com.fabcoders.brunchbites.model.model.getDaily

data class getDaily_responce(
    val `data`: List<Data>,
    val message: String,
    val status: String
)