package com.fabcoders.brunchbites.Model.Registration

data class registeration_responce(
    val message: String,
    val status: String,
    val userid: Int
)