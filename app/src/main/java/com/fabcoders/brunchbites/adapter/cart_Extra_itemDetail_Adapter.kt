package com.fabcoders.brunchbites.Adapter

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.Model.Address.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.cart.Extraitem
import com.fabcoders.brunchbites.namesVariable
import kotlinx.android.synthetic.main.content_mycart_alert_extraitem.view.*


class cart_Extra_itemDetail_Adapter(val partItemList: ArrayList<Extraitem>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): cart_Extra_itemDetail_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_mycart_alert_extraitem, parent, false)
        return cart_Extra_itemDetail_Adapter.PartViewHolder(view)




    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
//        var checkedPosition = -1


        holder.itemView.extra_itemRate.text = part.rate
        holder.itemView.Extraitem_name.text = part.productname





        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

//        cart_itemDetail_Adapter.mClickListener2 = listener
//        holder.itemView.container.setOnClickListener { view ->
//            cart_itemDetail_Adapter.mClickListener2?.clickToIntent2(position)


//            holder.itemView.check_mark.setVisibility(View.VISIBLE);
//            if (checkedPosition != position) {
//                notifyItemChanged(checkedPosition);
//                checkedPosition = position;
//            }
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))


//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


//        AddressAdapter.mClickListener = listener
//        holder.itemView.btnDelete.setOnClickListener { view ->
//            AddressAdapter.mClickListener?.clickToIntent(position)
////            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


//        cart_itemDetail_Adapter.mClickListener = listener
//        holder.itemView.btnEdit.setOnClickListener { view ->
//            cart_itemDetail_Adapter.mClickListener?.clickToIntent(position)
////            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {






        init {

        }

    }


    companion object {

    }

    interface ItemClickListener
    {

    }

}