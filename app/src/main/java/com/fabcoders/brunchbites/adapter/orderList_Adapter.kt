package com.fabcoders.brunchbites.Adapter

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.cart.DataX
import kotlinx.android.synthetic.main.item_address.view.*
import kotlinx.android.synthetic.main.item_order_header.view.*


class orderList_Adapter(val partItemList: ArrayList<DataX>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): orderList_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.item_order_header, parent, false)
        return orderList_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        var checkedPosition = -1

        holder.itemView.order.text ="ORDER NO "+part.orderid
        holder.itemView.total.text = part.total
        holder.itemView.date.text = part.entrydatetime



        orderList_Adapter.mClickListener = listener
        holder.itemView.view_details.setOnClickListener { view ->
            orderList_Adapter.mClickListener?.clickToIntent(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {






        init {

        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
    }

}