package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.getDaily.Data
import kotlinx.android.synthetic.main.content_choose_your_plan.view.*


class choose_plan_Adapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): choose_plan_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_choose_your_plan, parent, false)
        return choose_plan_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]

        val duration = part.duration
        holder.itemView.vegitem.text = part.vegrates.rate
        holder.itemView.nonvegitem.text = part.nonvegrates.rate

        if (duration.equals("D")){
            holder.itemView.dailyTag.text ="Daily"
            holder.itemView.days_week.text ="1 meal"
        }else if (duration.equals("W")){
            holder.itemView.dailyTag.text ="Weekly"
            holder.itemView.days_week.text ="6 meals"
        }else if (duration.equals("M")) {
            holder.itemView.dailyTag.text ="Monthly"
            holder.itemView.days_week.text ="24 meals"
        }


        if (checkedPosition == -1) {
            holder.itemView.check_mark.setVisibility(View.GONE);
        } else {
            if (checkedPosition == position) {
                holder.itemView.check_mark.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.check_mark.setVisibility(View.GONE);
            }
        }

        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        choose_plan_Adapter.mClickListener = listener
        holder.itemView.choose_plan_l4.setOnClickListener { view ->
            choose_plan_Adapter.mClickListener?.clickToIntent(position)

            holder.itemView.check_mark.setVisibility(View.VISIBLE);
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition);
                checkedPosition = position;
            }
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))



        }



//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var veg: TextView
        var Nveg: TextView
        var daily: TextView


        init {
            veg = itemView.findViewById(R.id.vegitem)
            Nveg = itemView.findViewById(R.id.nonvegitem)
            daily = itemView.findViewById(R.id.dailyTag)

        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
    }


}