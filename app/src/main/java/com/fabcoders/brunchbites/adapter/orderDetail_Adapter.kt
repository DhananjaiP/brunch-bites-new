package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.cart.DataXX

import kotlinx.android.synthetic.main.content_categoires.view.*
import kotlinx.android.synthetic.main.content_mybasket_item.view.*
import kotlinx.android.synthetic.main.content_mybasket_item.view.cartProduct_name
import kotlinx.android.synthetic.main.content_mybasket_item.view.cartProduct_total
import kotlinx.android.synthetic.main.content_orderdetail_item.view.*


class orderDetail_Adapter(val partItemList: ArrayList<DataXX>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): orderDetail_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_orderdetail_item, parent, false)
        return orderDetail_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        val checkedPosition = -1

        if (part.duration.equals("W"))
        {
            holder.itemView.orderDetail_day.text = "Week"
        }else if (part.duration.equals("M")){
            holder.itemView.orderDetail_day.text = "Month"
        }else{
            holder.itemView.orderDetail_day.text = "Daily"
        }

        holder.itemView.cartProduct_name.text = part.subcategoryname
        holder.itemView.cartProduct_total.text = "₹ "+part.subtotal
        holder.itemView.orderDetail_gst.text = "₹ "+part.gstamount
        holder.itemView.orderDetail_total.text = "₹ "+part.total

        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

//        orderDetail_Adapter.mClickListener = listener
//        holder.itemView.categories_menu.setOnClickListener { view ->
//            orderDetail_Adapter.mClickListener?.clickToIntent(position)
////            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


//        var titleTV: TextView



        init {
//            titleTV = itemView.findViewById(R.id.categories_menu)
        }

    }


    companion object {
//        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
//        fun clickToIntent(position: Int)
    }


}