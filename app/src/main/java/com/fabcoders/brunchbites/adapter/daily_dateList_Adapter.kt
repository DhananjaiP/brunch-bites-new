package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.Model.calendar.Daily_model
import com.fabcoders.brunchbites.Model.calendar.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.getStringSharedPref
import com.fabcoders.brunchbites.namesVariable
import kotlinx.android.synthetic.main.content_datelist.view.*


class daily_dateList_Adapter(val partItemList: ArrayList<Daily_model>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): daily_dateList_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_datelist, parent, false)
        return daily_dateList_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
//        val checkedPosition = -1


        if (getStringSharedPref(namesVariable.VN_SWITCH,this.context).equals("V")){
            holder.itemView.veg_Switch.isChecked = false
            holder.itemView.veg_Switch.setBackgroundResource(R.color.green)
            part.veg_nonVeg = false
        }else{
            holder.itemView.veg_Switch.isChecked = true
            holder.itemView.veg_Switch.setBackgroundResource(R.color.red)
            part.veg_nonVeg = true
        }


//        if( holder.itemView.veg_Switch.isChecked){
//            holder.itemView.veg_Switch.setBackgroundResource(R.color.red)
//            holder.itemView.veg_Switch.isChecked = true
//        }else{
//            holder.itemView.veg_Switch.setBackgroundResource(R.color.green)
//            holder.itemView.veg_Switch.isChecked = false
//        }

        holder.itemView.dateList_date.text = part.date

        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

//        dateList_Adapter.mClickListener = listener
//        holder.itemView.veg_Switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
//            // do something, the isChecked param will be
//            if( holder.itemView.veg_Switch.isChecked){
//                holder.itemView.veg_Switch.setBackgroundResource(R.color.green)
//                holder.itemView.veg_Switch.isChecked = true
//            }else{
//                holder.itemView.veg_Switch.setBackgroundResource(R.color.red)
//                holder.itemView.veg_Switch.isChecked = false
//            }
//
//
//            // true if the switch is in the On position
//            mClickListener?.clickToIntent(position,isChecked)
//
//
//        })



        daily_dateList_Adapter.mClickListener = listener
        holder.itemView.veg_Switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener
        { buttonView, isChecked ->
            // do something, the isChecked param will be
            // true if the switch is in the On position
            mClickListener?.clickToIntent(position,isChecked)

            if( holder.itemView.veg_Switch.isChecked){
                holder.itemView.veg_Switch.setBackgroundResource(R.color.red)
                holder.itemView.veg_Switch.isChecked = true
            }else{
                holder.itemView.veg_Switch.setBackgroundResource(R.color.green)
                holder.itemView.veg_Switch.isChecked = false
            }

        })

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var titleTV: TextView



        init {
            titleTV = itemView.findViewById(R.id.dateList_date)
        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int, checked: Boolean)
    }


}