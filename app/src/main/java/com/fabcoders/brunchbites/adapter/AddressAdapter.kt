package com.fabcoders.brunchbites.Adapter

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.Model.Address.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.getStringSharedPref
import com.fabcoders.brunchbites.namesVariable
import kotlinx.android.synthetic.main.content_choose_your_school.view.*
import kotlinx.android.synthetic.main.item_address.view.*
import kotlinx.android.synthetic.main.item_address.view.check_mark


class AddressAdapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.item_address, parent, false)
        return AddressAdapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        var checkedPosition = -1

        holder.itemView.person.text = getStringSharedPref(namesVariable.PERSON_NAME,this.context)
        holder.itemView.address.text = part.address1
        holder.itemView.landmarkTV.text = part.landmark
        holder.itemView.landmark.visibility = View.GONE
        holder.itemView.mobile.visibility = View.GONE
        holder.itemView.mobile_label.visibility = View.GONE


        if (checkedPosition == -1) {
            holder.itemView.check_mark.setVisibility(View.GONE);
        } else {
            if (checkedPosition == position) {
                holder.itemView.check_mark.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.check_mark.setVisibility(View.GONE);
            }
        }


        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        AddressAdapter.mClickListener2 = listener
        holder.itemView.container.setOnClickListener { view ->
            AddressAdapter.mClickListener2?.clickToIntent2(position)


            holder.itemView.check_mark.setVisibility(View.VISIBLE);
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition);
                checkedPosition = position;
            }
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


        AddressAdapter.mClickListener_delete = listener
        holder.itemView.btnDelete.setOnClickListener { view ->
            AddressAdapter.mClickListener_delete?.clickToIntent_delete(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }


        AddressAdapter.mClickListener = listener
        holder.itemView.btnEdit.setOnClickListener { view ->
            AddressAdapter.mClickListener?.clickToIntent(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {






        init {

        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
        var mClickListener2: ItemClickListener? = null
        var mClickListener_delete: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
        fun clickToIntent2(position: Int)
        fun clickToIntent_delete(position: Int)
    }

}