package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.MainActivity
import com.fabcoders.brunchbites.Model.Slider.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.loadImage
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_slider_home.view.*

class SliderAdapter(private val context: Context, private val list: ArrayList<Data>, private val listener: Listener) : androidx.viewpager.widget.PagerAdapter() {
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imgs = "http://Machotelslimited.com/brunchnew/"
        val view: ViewGroup = LayoutInflater.from(context).inflate(R.layout.item_slider_home, container, false) as ViewGroup
        if (position<=count && list[position].sliderimage!=null){
            Picasso.get().load(imgs+list[position].sliderimage).fit()
                .placeholder(R.drawable.progress_animation)
                .memoryPolicy(MemoryPolicy.NO_CACHE).into(view.sliderImage)
        }

        view.setOnClickListener { listener.onClick(position) }
        container.addView(view)
        return view
    }

    override fun getCount(): Int = list.size

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as FrameLayout)
    }

    interface Listener {
        fun onClick(pos: Int)
    }
}