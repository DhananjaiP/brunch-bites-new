package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.Products.Data
import kotlinx.android.synthetic.main.content_choose_your_school.view.*


class choose_extraItem_Adapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): choose_extraItem_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_choose_your_school, parent, false)
        return choose_extraItem_Adapter.PartViewHolder(view)
    }


    override fun getItemCount(): Int {
        return partItemList.size
    }



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]


        holder.itemView.choose_school_tv.text = part.productname


        if (checkedPosition == -1) {
            holder.itemView.check_mark.setVisibility(View.GONE);
        } else {
            if (checkedPosition == position) {
                holder.itemView.check_mark.setVisibility(View.VISIBLE);
            }
            else {
                holder.itemView.check_mark.setVisibility(View.GONE);
            }
        }


        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)
        choose_extraItem_Adapter.mClickListener = listener
       holder.itemView.choose_school_tv.setOnClickListener { view ->
           choose_extraItem_Adapter.mClickListener?.clickToIntent(position)

            holder.itemView.check_mark.setVisibility(View.VISIBLE);
            if (checkedPosition != position) {
//                notifyItemChanged(checkedPosition);
                checkedPosition = position;
            }

        }



//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var titleTV: TextView
        val clickview : ImageView


        init {
            titleTV = itemView.findViewById(R.id.choose_school_tv)
            clickview = itemView.findViewById(R.id.check_mark)
        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
    }


}