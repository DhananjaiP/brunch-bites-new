package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.cart.Data
import com.fabcoders.brunchbites.namesVariable
import com.fabcoders.brunchbites.setStringSharedPref
import kotlinx.android.synthetic.main.content_choose_your_school.view.*
import kotlinx.android.synthetic.main.content_mybasket_item.view.*
import okhttp3.internal.threadName


class cartProduct_Adapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): cartProduct_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_mybasket_item, parent, false)
        return cartProduct_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        val checkedPosition = -1
        holder.itemView.cartProduct_name.text = part.subcategoryname
        setStringSharedPref(namesVariable.CART_SUBCAT,part.subcategoryname,this.context)
        holder.itemView.cartProduct_total.text = part.subtotal

        if (part.duration.equals("W"))
        {
            holder.itemView.cartProduct_week.text = "Week"
        }else if (part.duration.equals("M")){
            holder.itemView.cartProduct_week.text = "Month"
        }else{
            holder.itemView.cartProduct_week.text = "Daily"
        }


        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        cartProduct_Adapter.mClickListener = listener
        holder.itemView.cartProduct_delete.setOnClickListener { view ->
            cartProduct_Adapter.mClickListener?.clickToIntent(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

        cartProduct_Adapter.mClickListener1 = listener
        holder.itemView.itemDetails.setOnClickListener { view ->
            cartProduct_Adapter.mClickListener1!!.clickToIntent1(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


//        var titleTV: TextView
//


        init {
//            titleTV = itemView.findViewById(R.id.choose_school_tv)
        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
        var mClickListener1: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
        fun clickToIntent1(position: Int)
    }


}