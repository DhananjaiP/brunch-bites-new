package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.Products.Data
import kotlinx.android.synthetic.main.calender.view.*


class product_Adapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): product_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.calender, parent, false)
        return product_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        val checkedPosition = -1
//        holder.itemView.choose_school_tv.text = part.areaname

        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        product_Adapter.mClickListener = listener
        holder.itemView.choose_cal_vn.setOnCheckedChangeListener { buttonView, isChecked ->

            mClickListener?.clickToIntent(position,isChecked)

        }

//        holder.itemView.choose_cal_vn.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
//            // do something, the isChecked param will be
//            // true if the switch is in the On position
//            dateList_Adapter.mClickListener?.clickToIntent(position,isChecked)
//        })


//        product_Adapter.mClickListener = listener
//        holder.itemView.choose_school_tv.setOnClickListener { view ->
//            product_Adapter.mClickListener?.clickToIntent(position)
////            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var titleTV: TextView



        init {
            titleTV = itemView.findViewById(R.id.choose_school_tv)
        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int, checked: Boolean)
    }


}