package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.Categories.DataXX

import kotlinx.android.synthetic.main.content_categoires.view.*


class categoryt_product_Adapter(val partItemList: ArrayList<DataXX>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): categoryt_product_Adapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.content_categoires, parent, false)
        return categoryt_product_Adapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]
        val checkedPosition = -1

        val vale = part.categoryname
        holder.itemView.categories_menu.text = part.categoryname

        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        categoryt_product_Adapter.mClickListener = listener
        holder.itemView.categories_menu.setOnClickListener { view ->
            categoryt_product_Adapter.mClickListener?.clickToIntent(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

        if ( vale.equals("Schools")){
            holder.itemView.categories_menu.setBackgroundResource(R.drawable.school)
        }else  if (  vale.equals("Breakfast")){
            holder.itemView.categories_menu.setBackgroundResource(R.drawable.corporate)
        }else  if (  vale.equals("Lunch")){
            holder.itemView.categories_menu.setBackgroundResource(R.drawable.thaal_box)
        }else  if (  vale.equals("Dinner")){
            holder.itemView.categories_menu.setBackgroundResource(R.drawable.dinner)
        }else  if (  vale.equals("Asian")){
            holder.itemView.categories_menu.setBackgroundResource(R.drawable.events)
        }

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var titleTV: TextView



        init {
            titleTV = itemView.findViewById(R.id.categories_menu)
        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
    }


}