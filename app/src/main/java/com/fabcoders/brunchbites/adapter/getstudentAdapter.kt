package com.fabcoders.brunchbites.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.model.model.Student.Data
import kotlinx.android.synthetic.main.item_address.view.*


class getstudentAdapter(val partItemList: ArrayList<Data>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var checkedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): getstudentAdapter.PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.item_address, parent, false)
        return getstudentAdapter.PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val part = partItemList[position]

        holder.itemView.person.text = part.studentname
        holder.itemView.address.text = part.classdetail
        holder.itemView.locality.visibility = View.GONE
        holder.itemView.landmarkTV.visibility = View.GONE
        holder.itemView.landmark.visibility = View.GONE
        holder.itemView.mobile.visibility = View.GONE
        holder.itemView.mobile_label.visibility = View.GONE


        if (checkedPosition == -1) {
            holder.itemView.check_mark.setVisibility(View.GONE);
        } else {
            if (checkedPosition == position) {
                holder.itemView.check_mark.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.check_mark.setVisibility(View.GONE);
            }
        }



        //http://3.135.185.114/goancart-admin-api/assets/

//       Picasso.get().load(part.Icon_Url).fit().placeholder(R.drawable.loader).into(holder.itemView.content_product)

        getstudentAdapter.mClickListener = listener
        holder.itemView.btnDelete.setOnClickListener { view ->
            getstudentAdapter.mClickListener?.clickToIntent(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }


        getstudentAdapter.mClickListener2 = listener
        holder.itemView.btnEdit.setOnClickListener { view ->
            getstudentAdapter.mClickListener2?.clickToIntent2(position)
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

        getstudentAdapter.mClickListener3 = listener
        holder.itemView.container.setOnClickListener { view ->
            getstudentAdapter.mClickListener3?.clickToIntent3(position)


            holder.itemView.check_mark.setVisibility(View.VISIBLE);
            if (checkedPosition != position) {
                notifyItemChanged(checkedPosition);
                checkedPosition = position;
            }
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
        }

//        if(position == position){
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#ffffff"))
//        }else{
//            holder.itemView.select_product.setBackgroundColor(Color.parseColor("#F49431"))
//        }


    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {






        init {

        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
        var mClickListener2: ItemClickListener? = null
        var mClickListener3: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
        fun clickToIntent2(position: Int)
        fun clickToIntent3(position: Int)
    }


}