package com.fabcoders.brunchbites

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Handler
import android.os.StrictMode
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.fabcoders.maidan.retrofit.APIClient
import com.squareup.picasso.Picasso
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Context?.toast(text: CharSequence) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
fun logd(text: String, tag: String = "UTIL") = Log.d(tag, text)
fun Double.formatToPrice(places: Int = 2) = "₹" + String.format("%.2f", this)
//fun ArrayList<String>.clogList(tag: String) = Log.d(tag, Gson().toJson(this))

val service = APIClient.makeRetrofitService()


fun Context.printHashKey() {
    try {
        val info = this.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
        for (signature in info.signatures) {
            val md = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            val hashKey = String(Base64.encode(md.digest(), 0))
            Log.i(packageName, "printHashKey() Hash Key: $hashKey")
        }
    } catch (e: NoSuchAlgorithmException) {
        Log.i(packageName, e.toString())
    } catch (e: Exception) {
        Log.i(packageName, e.toString())
    }
}


fun String.toReadableDate(): String {
    val sqlFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
    val dateFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())

    try {
        val date = sqlFormat.parse(this)
        return dateFormat.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return "no date"
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show(time: Long = 4000) {
    visible()
    Handler().postDelayed({ gone() }, time)
}

//fun Product.getTotalPrice(): Double {
//    return this.price * this.qtyAdded * (1 + 0.01 * (this.cgst + this.sgst))
//}

////gst to percent
//fun Double.toPercent(): String = "${this}%"
//
//fun ArrayList<MyBasket.ProdQuant>.toJsonArray(): String = Gson().toJson(this)
//
//fun ArrayList<Basket.ProdQuant>.toBasketJsonArray(): String = Gson().toJson(this)
//
//fun Product.toJsonArray(): String = "[${Gson().toJson(this)}]"
//
//fun String.toDate(): Date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(this)
//
//fun Date.readableFull(): String = SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.ENGLISH).format(this)
//
//fun Any.log(tag: String = "tag") = Log.d(tag, this.toString())
//fun Any.loge(tag: String = "tag") = Log.e(tag, this.toString())
//
//fun Int.toDp(ctx: Context): Int {
//    val density = ctx.resources
//            .displayMetrics
//            .density
//    return Math.round(this * density)
//}
//
//fun Context.isNetworkAvailable(): Boolean {
//    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
//    return activeNetwork?.isConnectedOrConnecting == true
//}
//
fun ImageView.loadImage(url: String, circular: Boolean = false) {
    if (circular) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.loader)
//                .transform(CircleTransform())
                .into(this)
    } else {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.loader)
                .into(this)
    }

}
//
//fun stringToDate(time: String): Calendar {
//    val calendar = Calendar.getInstance()
//    calendar.time = SimpleDateFormat("HH:mm:ss").parse(time)
//    return calendar
//}
//
//fun dateToTimestring(calendar: Calendar): String {
//    return SimpleDateFormat("HH:mm").format(calendar.time)
//}
//
//fun initQuantity(): MutableList<String> {
//    var quantityList: MutableList<String> = ArrayList()
//    if (quantityList.isEmpty()){
//        quantityList.add("100 gms")
//        quantityList.add("250 gms")
//        quantityList.add("500 gms")
//        quantityList.add("1.0 kg")
//        quantityList.add("2.0 kg")
//        quantityList.add("3.0 kg")
//        quantityList.add("5.0 kg")
//        quantityList.add("10.0 kg")
////        quantityList.add("20.0 kg")
//    }
//    return quantityList
//}
//
//fun initQuantityValue(): MutableList<String> {
//    var quantityListValue: MutableList<String> = ArrayList()
//    if(quantityListValue.isEmpty()){
//        quantityListValue.add("0.1")
//        quantityListValue.add("0.25")
//        quantityListValue.add("0.5")
//        quantityListValue.add("1.0")
//        quantityListValue.add("2.0")
//        quantityListValue.add("3.0")
//        quantityListValue.add("5.0")
//        quantityListValue.add("10.0")
////        quantityListValue.add("20.0")
//    }
//    return quantityListValue
//}
//
//@SuppressLint("ResourceAsColor")
//fun customToast(message: String, context: Context) {
//    val toast: Toast = Toast.makeText(context, " "+message, Toast.LENGTH_SHORT)
//    val view: View = toast.getView()
//    view.setBackgroundResource(R.color.purple)
//    val text = view.findViewById(android.R.id.message) as TextView
//    text.setTextColor(Color.parseColor("#ffffff"))
//    text.setText(" "+message)
//    text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_big_tick,0,0,0)
//    text.gravity = Gravity.CENTER_VERTICAL
//    toast.show()
//}


private fun hasNetworkAvailable(context: Context): Boolean {
    val service = Context.CONNECTIVITY_SERVICE
    val manager = context.getSystemService(service) as ConnectivityManager?
    val network = manager?.activeNetworkInfo
    Log.d("", "hasNetworkAvailable: ${(network != null)}")
    return (network != null)
}

fun hasInternetConnected(context: Context): Boolean {
    val REACHABILITY_SERVER = "https://www.google.com"
    // Disable the `NetworkOnMainThreadException` and make sure it is just logged.
    StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build())
    if (hasNetworkAvailable(context)) {
        try {
            val connection = URL(REACHABILITY_SERVER).openConnection() as HttpURLConnection
            connection.setRequestProperty("User-Agent", "Test")
            connection.setRequestProperty("Connection", "close")
            connection.connectTimeout = 1000
            connection.connect()
            return (connection.responseCode == 200)
        } catch (e: IOException) {
            Log.e("", "Error checking internet connection", e)
        }
    } else {
        Log.w("", "No network available!")
    }
    Log.d("", "hasInternetConnected: false")
    return false
}

fun getStringSharedPref(key:String,context: Context): String {
    var sharedPreferences: SharedPreferences
    sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0)
    return sharedPreferences.getString(key, "").toString()
}

fun setStringSharedPref(key:String,value_s:String,context: Context){
    var sharedPreferences: SharedPreferences
    sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0)
    val editor = sharedPreferences.edit()
    editor.putString(key,value_s)
    editor.apply()
}
