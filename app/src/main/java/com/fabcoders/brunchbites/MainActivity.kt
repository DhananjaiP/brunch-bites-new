package com.fabcoders.brunchbites

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.fabcoders.brunchbites.Adapter.SliderAdapter
import com.fabcoders.brunchbites.Adapter.categoryt_product_Adapter
import com.fabcoders.brunchbites.Model.Slider.Data
import com.fabcoders.brunchbites.fragment.AddressListFragment
import com.fabcoders.brunchbites.model.model.Categories.DataXX
import com.fabcoders.brunchbites.model.model.Categories.categoriesRequest
import com.fabcoders.brunchbites.ui.Orders_List.OrderList
import com.fabcoders.brunchbites.ui.Profile.Profile
import com.fabcoders.brunchbites.ui.choose_school.choose_school
import com.fabcoders.brunchbites.ui.login.LogIn
import com.fabcoders.brunchbites.ui.menu.Menu
import com.fabcoders.brunchbites.ui.myCart.myCart
import com.fabcoders.maidan.retrofit.APIClient
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.alerdialog_forgotpass.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class MainActivity : AppCompatActivity(), SliderAdapter.Listener {
    lateinit var im1 :ImageView
    lateinit var im2 :ImageView
    lateinit var im3 :ImageView
    lateinit var im4 :ImageView
    lateinit var homeSlider: ViewPager
    private var mDrawer: DrawerLayout? = null
    private var toolbar: Toolbar? = null
    private lateinit var nvDrawer:NavigationView
    private var drawerToggle: ActionBarDrawerToggle? = null
    val service = APIClient.makeRetrofitService()



    private var bannerList: ArrayList<Data> = ArrayList()
    private var sliderAdapter = SliderAdapter(this, bannerList, this)



    lateinit var categoryAdapters: categoryt_product_Adapter
    lateinit var categoryItems: ArrayList<DataXX>
    private var handler: Handler = Handler()
    lateinit var rv : RecyclerView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //getWindow().setExitTransition(null);


        // Set a Toolbar to replace the ActionBar.
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        // This will display an Up icon (<-), we will replace it with hamburger later

        // This will display an Up icon (<-), we will replace it with hamburger later
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Find our drawer view

        // Find our drawer view
        mDrawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val nvDrawer: NavigationView = findViewById(R.id.nav_view)
        setupDrawerContent(nvDrawer)
        mDrawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawerToggle = setupDrawerToggle()
        drawerToggle!!.isDrawerIndicatorEnabled = true
        drawerToggle!!.syncState()
        mDrawer!!.addDrawerListener(drawerToggle!!)
//        bannerDao = AppDb.getInstance(this)?.bannerDao()


        findviews()

        categoryItems=ArrayList()

        initRecycler1()

        getCategorys()
//        initImageSlider()

        getSlider()

        homeSlider.adapter = sliderAdapter

//        im1.setOnClickListener {
//            startActivity(Intent(this, Menu::class.java))
//        }
//
//        im2.setOnClickListener {
//            val intent = Intent(this, Menu::class.java)
//            val strName: String? = "corporate"
//            intent.putExtra("STRING_I_NEED", strName)
//            startActivity(intent)
//        }
//
//        im3.setOnClickListener {
//            val intent = Intent(this, Menu::class.java)
//            val strName2: String? = "thaal"
//            intent.putExtra("THAAL_I_NEED", strName2)
//            startActivity(intent)
//        }
//
//        im4.setOnClickListener {
//            val intent = Intent(this, Menu::class.java)
//            val strName3: String? = "events"
//            intent.putExtra("EVENTS_I_NEED", strName3)
//            startActivity(intent)
//        }




    }




    //function for Drawable Menu(onclick of the Drawer event)
    private fun setupDrawerToggle(): ActionBarDrawerToggle? {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close)
    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            selectDrawerItem(menuItem)
            true
        }
    }

    fun selectDrawerItem(menuItem: MenuItem) {
        when (menuItem.itemId) {

            R.id.nav_my_account -> {
                val p = Intent(this, Profile::class.java)
                p.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(p)
//                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_orders -> {
                val o = Intent(this, OrderList::class.java)
                o.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(o)
//                Toast.makeText(this, "My Orders", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_address -> {
                val p = Intent(this, AddressListFragment::class.java)
                p.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(p)
//                Toast.makeText(this, "My Address", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_logout -> {
                setStringSharedPref(namesVariable.USERID,"",this)
                setStringSharedPref(namesVariable.PASSWORD,"",this)
                val intent = Intent(this, LogIn::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
//                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show()
            }
        }

        // Highlight the selected item has been done by NavigationView
        menuItem.isCheckable = true
        // Set action bar title
        mDrawer!!.closeDrawers()
    }

    //function for Toolbar Menu(onclick of the Toolbar event)
    override fun onCreateOptionsMenu(menu: android.view.Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cart, menu)
        val cartMenuItem = menu!!.findItem(R.id.action_cart)
//        val cartCountView = MenuItemCompat.getActionView(cartMenuItem)
        val cartCountView = menu.findItem(R.id.action_cart).actionView
        val cartLabel = cartCountView.findViewById(R.id.cart_badge) as TextView

        var count = getStringSharedPref(namesVariable.CART_COUNT, this)

        if(count.equals(""))
        {
            count = "0"
        }

        val temp: Int = count.toInt()
        cartLabel.text = temp.toString()
        if (temp > 0)
            cartLabel.visible()
        cartCountView.setOnClickListener { onOptionsItemSelected(cartMenuItem) }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (drawerToggle!!.onOptionsItemSelected(item)) {
            return true
        }
        when (item.itemId) {
            R.id.action_cart -> {
                val no = Intent(this, myCart::class.java)
                no.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(no)
//                Toast.makeText(this, "Cart", Toast.LENGTH_SHORT).show()
            }
            android.R.id.home -> {
                mDrawer!!.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun getSlider() {


//        sliderAdapter = SliderAdapter(this!!, bannerList,object :SliderAdapter.ItemClickListener{
//            override fun onClick(pos: Int) {
//                clickIntentType()
//            }
//        })
//
//        homeSlider.layoutManager = GridLayoutManager(this,1, GridLayoutManager.HORIZONTAL,false)
//        homeSlider.setHasFixedSize(true)

        CoroutineScope(Dispatchers.IO).launch {

            val response = service.getSLiders()
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            bannerList.clear()
                            val result = response.body()
                            for (x in result!!.data.indices){
                                bannerList.add(result.data[x])
                            }

                            sliderAdapter.notifyDataSetChanged()
                            handler.post(runnable)

//                            homeSlider.adapter = sliderAdapter
//                            sliderAdapter.notifyDataSetChanged()
//
//
//                            if (bannerList.size>0){
//                                homeSlider.smoothScrollToPosition(0)
//                                homeSlider.offsetChildrenHorizontal(3)
//                            }

//                            handler = Handler()
//                            val r: Runnable = object : Runnable {
//                                override fun run() {
//                                    for (x in 0 until  bannerList.size+1){
//                                        if (x<bannerList.size){
//                                            homeSlider.smoothScrollToPosition(x)
//                                            handler.postDelayed(this, 1000)
//                                        } else{
//                                            homeSlider.smoothScrollToPosition(0)
//                                            handler.postDelayed(this, 1000)
//                                        }
//                                    }
//                                }
//                            }
//                            handler.postDelayed(r, 1000)
//                            doAsync {
//                                bannerDao?.deleteAll()
//                                bannerDao?.insertAll(sliderlist)
//                            }

//                            val viewPager = findViewById<ViewPager>(R.id.homeSlider)
//                            val adapter = ViewPagerAdapter(this, sl)
//                            viewPager.adapter = adapter


//                            Toast.makeText(this@MainActivity, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@MainActivity, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    private fun clickIntentType() {

    }




    private fun initRecycler1() {
        rv=findViewById(R.id.categories_recycler)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        rv.smoothScrollToPosition(1)
        categoryAdapters= categoryt_product_Adapter(categoryItems, object : categoryt_product_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }



    private fun findviews() {
        homeSlider = findViewById<ViewPager>(R.id.homeSlider)
//        im1 = findViewById(R.id.school_lay)
//        im2 = findViewById(R.id.corporate_lay)
//        im3 = findViewById(R.id.thaal_lay)
//        im4 = findViewById(R.id.evnts)
    }


    private fun getCategorys() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = categoriesRequest()
            logReq.userid = getStringSharedPref(namesVariable.USERID,this@MainActivity)
            val response = service.getCategories(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= categoryt_product_Adapter(categoryItems, object : categoryt_product_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val pick_str = result.data[position].categoryid
                                    val cattyp_str = result.data[position].cattype
                                    setStringSharedPref(namesVariable.CATEGORY,pick_str,this@MainActivity)
                                    setStringSharedPref(namesVariable.CATEGORY_TYPE,cattyp_str,this@MainActivity)
                                    val image_link = result.data[position].imglink
                                    setStringSharedPref(namesVariable.IMAGE_LINK,image_link,this@MainActivity)
                                    setStringSharedPref(namesVariable.MENU_NAME,result.data[position].categoryname,this@MainActivity)
//                                    toast(pick_str)

                                    if (result.data[position].cattype.equals("")){
//                                        startActivity(intent)
                                        startActivity(Intent(this@MainActivity,Menu::class.java))
                                    }else{
                                        startActivity(Intent(this@MainActivity,choose_school::class.java))
                                    }

                                }
                            }, this@MainActivity)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@MainActivity, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@MainActivity, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }



    private val runnable = object : Runnable {
        override fun run() {
            var currentItem = homeSlider.currentItem
            if (currentItem < sliderAdapter.count - 1) {
                homeSlider.currentItem = ++currentItem
            } else {
                homeSlider.setCurrentItem(0, false)
            }
            handler.postDelayed(this, Constants.HOME_SLIDER_SLIDE_DELAY)
        }
    }

    override fun onClick(pos: Int) {
        TODO("Not yet implemented")
    }
//
//    override fun onClick(pos: Int) {
//        TODO("Not yet implemented")
//    }

//    override fun onPostResume() {
//        getSlider()
//        super.onPostResume()
//    }



    override fun onBackPressed() {
        val dialogBuilder = AlertDialog.Builder(this@MainActivity)
        val view = LayoutInflater.from(this@MainActivity).inflate(R.layout.alerdialog_forgotpass, null)
        dialogBuilder.setView(view)
        view.forgotpaas_text.setText("Do you want to Exit ?")
        view.forgotpaas_text.textAlignment = View.TEXT_ALIGNMENT_TEXT_START
        dialogBuilder.setPositiveButton("Yes",DialogInterface.OnClickListener(){
                dialogInterface, i ->
            finish()
        })
        dialogBuilder.setNegativeButton("Cancel",DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.cancel()
        })

//        dialogBuilder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.textColorDisable))
        dialogBuilder.show()
    }



}