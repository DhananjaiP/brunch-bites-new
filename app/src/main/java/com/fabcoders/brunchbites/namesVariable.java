package com.fabcoders.brunchbites;

public class namesVariable {

    public static final String USERINPUTID="userinputid";
    public static final String PERSON_NAME="person_name";
    public static final String EMAIL="email";
    public static final String CATEGORY="Category";
    public static final String SUB_CATEGORY="Sub_Category";
    public static final String CATEGORY_TYPE="Category_type";
    public static final String USERID="userid";
    public static final String PHONE_NUMBER="Phone_Number";
    public static final String PASSWORD="Password";
    public static final String REGION_ID="region_id";
    public static final String SELECTED_DAY="selected_day";
    public static final String PRODCUTTYPE="producttype";
    public static final String DURATION="duration";
    public static final String IMAGE_LINK="image_link";
    public static final String DAYS="days";
    public static final String MENU_NAME="menu_name";

    public static final String STUDENT_ID="student_id";
    public static final String STUDENT_NAME="student_name";
    public static final String STUDENT_CLASS="student_class";
    public static final String EDIT_STUDENT_ID="edit_student_id";

    public static final String DATE_CHOOSED="date_choosed";
    public static final String MONTH_DAYS="month_days";

    public static final String ORDER_ID="order_id";
    public static final String CART_COUNT="order_id";

    public static final String ADDRESS_ID="address_id";

    public static final String GRAND_TOTAL="grand_total";
    public static final String FLAAG="flaag";
    public static final String LID="lid";
    public static final String ADDCART_LID="addcart_lid";
    public static final String PRODUCT_ID="product_id";

    public static final String VN_SWITCH="vn_switch";

    public static final String CART_SUBCAT="cart_subcat";
    public static final String COUPONCODE="coupon_code";
}
