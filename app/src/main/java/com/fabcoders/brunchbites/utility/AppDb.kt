package com.fabcoders.brunchbites.utility

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.fabcoders.brunchbites.BannerDao
import com.fabcoders.brunchbites.Model.Slider.Data
import com.fabcoders.brunchbites.Model.schoool_List.school_list_Responce

@Database(entities = [Data::class], version = 11)
abstract class AppDb : RoomDatabase() {


    abstract fun bannerDao(): BannerDao


    companion object {
        private var INSTANCE: AppDb? = null

        fun getInstance(context: Context): AppDb? {
            if (INSTANCE == null) {
                synchronized(AppDb::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDb::class.java, "vk_db001.db")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}