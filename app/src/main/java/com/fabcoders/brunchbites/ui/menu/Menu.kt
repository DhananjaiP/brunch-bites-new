package com.fabcoders.brunchbites.ui.menu

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.Sub_categoryt_product_Adapter
import com.fabcoders.brunchbites.Model.Categories.DataX
import com.fabcoders.brunchbites.model.model.Categories.sub_categoriesRequest
import com.fabcoders.brunchbites.ui.choose_plan.choose_plan
import com.fabcoders.brunchbites.utility.CustomScrollView
import com.fabcoders.maidan.retrofit.APIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.util.*

class Menu : AppCompatActivity() {
    lateinit var b1 :Button
    lateinit var li:LinearLayout
    lateinit var t1:TextView
    lateinit var t2:TextView
    lateinit var t3:TextView
    lateinit var tag:TextView
    lateinit var im:ImageView
    lateinit var im2:ImageView
    lateinit var l2:LinearLayout
    lateinit var rv : RecyclerView

    lateinit var cs : CustomScrollView

    val service = APIClient.makeRetrofitService()
    lateinit var categoryAdapters: Sub_categoryt_product_Adapter
    lateinit var categoryItems: ArrayList<DataX>
    lateinit var img: ImageView
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_school)
        setupToolbar()

        findViews()
        categoryItems=ArrayList()
        initRecycler1()
        getSubCategory()
//        Companion.UPLOADS_URL
        tag.setText(getStringSharedPref(namesVariable.MENU_NAME,this))
        val img_name = getStringSharedPref(namesVariable.IMAGE_LINK,this)
        val imgs = "http://Machotelslimited.com/brunchnew/"
        img.loadImage(imgs + img_name, true)


        cs.setOnTouchListener(View.OnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                cs.startScrollerTask()
            }
            false
        })



//        val newString: String?
//        newString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("STRING_I_NEED")
//        } else {
//            savedInstanceState.getSerializable("STRING_I_NEED") as String?
//        }
//
//        val thaalString: String?
//        thaalString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("THAAL_I_NEED")
//        } else {
//            savedInstanceState.getSerializable("THAAL_I_NEED") as String?
//        }
//
//
//        val eventString: String?
//        eventString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("EVENTS_I_NEED")
//        } else {
//            savedInstanceState.getSerializable("EVENTS_I_NEED") as String?
//        }
//
//       if (newString == "corporate"){
//           tag.text = "CORPORATE"
//           t2.visibility = View.GONE
//           t3.visibility = View.GONE
//           im.visibility = View.GONE
//           l2.setBackgroundResource(R.color.orange_7)
//           li.setBackgroundResource(R.drawable.orange_background)
//       }else if(thaalString == "thaal"){
//           tag.text = "THAAL BOX"
//           t2.visibility = View.GONE
//           t3.visibility = View.GONE
//           im.visibility = View.GONE
//           l2.setBackgroundResource(R.color.green1)
//           im2.setImageResource(R.drawable.thaal_background)
//           li.setBackgroundResource(R.drawable.green_background)
//       } else if(eventString == "events"){
//           tag.text = "EVENTS"
//           t2.visibility = View.GONE
//           t3.visibility = View.GONE
//           im.visibility = View.GONE
//           l2.setBackgroundResource(R.color.purple2)
//           im2.setImageResource(R.drawable.event_menu)
//           li.setBackgroundResource(R.drawable.purple_background)
//       }


//        b1.setOnClickListener {
////            if (newString == "corporate"){
////                val intent =  Intent(this, choose_school::class.java)
////                val strName: String? = "corporate"
////                intent.putExtra("STRING_CHOOSE_SCHOOL", strName)
////                startActivity(intent)
////            }else if(thaalString == "thaal"){
////                val intent =  Intent(this, choose_school::class.java)
////                val strName2: String? = "thaal"
////                intent.putExtra("THAAL_CHOOSE_SCHOOL", strName2)
////                startActivity(intent)
////            }else if(eventString == "events"){
////                val intent =  Intent(this, choose_school::class.java)
////                val strName3: String? = "events"
////                intent.putExtra("EVENTS_CHOOSE_SCHOOL", strName3)
////                startActivity(intent)
////            }
////            else{
////                startActivity(Intent(this,choose_school::class.java))
////            }
//
//        }
    }

    private fun initRecycler1() {
        rv=findViewById(R.id.recycler_subcategory)
        rv.layoutManager = LinearLayoutManager(this)
        rv.isNestedScrollingEnabled = false
//        rv.setHasFixedSize(true)
        categoryAdapters= Sub_categoryt_product_Adapter(categoryItems, object : Sub_categoryt_product_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this!!)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()

    }

    private fun getSubCategory() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = sub_categoriesRequest()
            logReq.catid = getStringSharedPref(namesVariable.CATEGORY,this@Menu)
            val response = service.getSubCategories(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= Sub_categoryt_product_Adapter(categoryItems, object : Sub_categoryt_product_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val pick_str = result.data[position].subcategoryid
                                    setStringSharedPref(namesVariable.SUB_CATEGORY,pick_str,this@Menu)
//                                    toast(pick_str)

                                    startActivity(Intent(this@Menu,choose_plan::class.java))
//                                    if (result.data[position].cattype.equals("")){
//                                        startActivity(Intent(this@Menu,Menu::class.java))
//                                    }else{
//                                        startActivity(Intent(this@Menu,choose_school::class.java))
//                                    }

                                }
                            },this@Menu!!)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@Menu, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@Menu, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun findViews() {
        img = findViewById(R.id.menu_image)
        cs = findViewById(R.id.customScroll_subCategory)
//        b1 = findViewById(R.id.menu_orderNow)
//        li = findViewById(R.id.lin)
//        t1 = findViewById(R.id.menu_title)
//        t2 = findViewById(R.id.menu_title2)
//        t3 = findViewById(R.id.menu_title3)
//        im = findViewById(R.id.menu_1)
//        im2 = findViewById(R.id.menu_img2)
        tag = findViewById(R.id.menu_tag)
//        l2 = findViewById(R.id.menu_l1)
    }
    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

}