package com.fabcoders.brunchbites.ui.Orders_List

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.categoryt_product_Adapter
import com.fabcoders.brunchbites.Adapter.orderList_Adapter
import com.fabcoders.brunchbites.model.model.Categories.DataXX
import com.fabcoders.brunchbites.model.model.Order.Order_Request
import com.fabcoders.brunchbites.model.model.Student.add_Student_Request
import com.fabcoders.brunchbites.model.model.cart.DataX
import com.fabcoders.brunchbites.ui.choose_school.choose_school
import com.fabcoders.brunchbites.ui.menu.Menu
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class OrderList : AppCompatActivity() {

    lateinit var categoryAdapters: orderList_Adapter
    lateinit var categoryItems: ArrayList<DataX>
    lateinit var rv : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_order_header)

        setupToolbar()
        categoryItems=ArrayList()
        initRecycler1()
        getdata()
    }

    private fun initRecycler1() {
        rv=findViewById(R.id.OrderrvList)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        rv.smoothScrollToPosition(1)
        categoryAdapters= orderList_Adapter(categoryItems, object : orderList_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }

    private fun getdata() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Order_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID,this@OrderList)
            val response = service.getOrdersList(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

//                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= orderList_Adapter(categoryItems, object : orderList_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val pick_str = result.data[position].orderid
                                    setStringSharedPref(namesVariable.ORDER_ID,pick_str,this@OrderList)

                                    startActivity(Intent(this@OrderList,OrderDetails::class.java))


                                }
                            }, this@OrderList)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@OrderList, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@OrderList, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}