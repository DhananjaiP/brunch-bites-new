package com.fabcoders.brunchbites.ui

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.Home.EventDecorator
import com.fabcoders.brunchbites.Home.HighlightWeekendsDecorator
import com.fabcoders.brunchbites.Model.calendar.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.logd
import com.fabcoders.brunchbites.model.model.calendar.getDates_Request
import com.fabcoders.brunchbites.toast
import com.fabcoders.maidan.retrofit.APIClient
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDate
import retrofit2.HttpException
import java.text.SimpleDateFormat

class cal: AppCompatActivity(){

    lateinit var calendarView: MaterialCalendarView
    lateinit var arrayListDateTime: ArrayList<Data>
    var dates = ""
    val service = APIClient.makeRetrofitService()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_calendar_view)
        arrayListDateTime=ArrayList()


        calendarView = findViewById(R.id.calendarView)

        calendarView.setOnDateChangedListener(OnDateSelectedListener { widget, date, selected ->
            dates="${date.day}-${date.month}-${date.year}"
            val parsedDateObj = SimpleDateFormat("dd-M-yyyy").parse(dates)
            dates = SimpleDateFormat("yyyy-MM-dd").format(parsedDateObj)
            toast(dates)
            loadTimeData()
        })

    }

    private fun loadTimeData() {
        CoroutineScope(Dispatchers.IO).launch {

            val reqval = getDates_Request()
            reqval.days = "6"
            reqval.fromdate = dates
            reqval.schoolid = "0"
            reqval.skipsaturday = "0"
            val response = service.getDates(reqval)
            logd(reqval.toString())
            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())

                            for (x in result!!.data.indices){
                                arrayListDateTime.add(result.data[x])
                            }

                            loadDateData()


                            Toast.makeText(this@cal, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@cal, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun loadDateData() {
        val calendarView = findViewById<MaterialCalendarView>(R.id.calendarView)
        val instance = LocalDate.now()
        calendarView.setSelectedDate(instance)
        calendarView.state().edit().setMinimumDate(instance).commit()
        calendarView.addDecorator(HighlightWeekendsDecorator())
        var arrayListname=ArrayList<CalendarDay>()
        if (!arrayListDateTime.isEmpty()){
            arrayListname.clear()
            arrayListDateTime.forEach{
                var date_now = it.date.toString()
                val parsedDateObj = SimpleDateFormat("yyyy-MM-dd").parse(date_now)
                date_now = SimpleDateFormat("dd-M-yyyy").format(parsedDateObj)
                var delimiter = "-"
                val parts = date_now.split(delimiter)
                val day = CalendarDay.from(LocalDate.of(parts[2].toInt(),parts[1].toInt(),parts[0].toInt()))
                arrayListname.add(day)
            }
        }
        calendarView.addDecorator(EventDecorator(Color.GREEN, arrayListname.toMutableList()))

    }
}