package com.fabcoders.brunchbites.ui.add_Extra_Item

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.choose_extraItem_Adapter
import com.fabcoders.brunchbites.Adapter.dateList_Adapter
import com.fabcoders.brunchbites.model.model.Products.Data
import com.fabcoders.brunchbites.model.model.Products.product_request
import com.fabcoders.brunchbites.ui.myCart.myCart
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class choose_extra_item : AppCompatActivity() {
    lateinit var radio_group: RadioGroup
    lateinit var b1: Button
    lateinit var l1: LinearLayout
    lateinit var l2: LinearLayout
    lateinit var l3: LinearLayout
    lateinit var l4: LinearLayout
    lateinit var l5: LinearLayout
    lateinit var l6: LinearLayout
    lateinit var tv: TextView
    lateinit var categoryAdapters: choose_extraItem_Adapter
    lateinit var categoryItems: ArrayList<Data>

    lateinit var rv : RecyclerView
    val users = ArrayList<String>()
    lateinit var  product :Array<*>

//
//    lateinit var categoryAdapters2: dateList_Adapter
//    lateinit var arrayListDateTime: ArrayList<com.fabcoders.brunchbites.Model.calendar.Data>
    lateinit var daysval: String
    lateinit var skip_sat: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_extra_item)
        setupToolbar()
        findViews()
//        arrayListDateTime = ArrayList()
//        logd(arrayListDateTime.toString())
        categoryItems=ArrayList()
        initRecycler1()


//        val bundle = this.intent.extras
//        if (bundle != null) {
//        val  product= bundle.getSerializable("product") as ArrayList<*>
//        logd(product.toString())
//        }


        val eventString = getStringSharedPref(namesVariable.DAYS, this)
        val montString = getStringSharedPref(namesVariable.DURATION, this)

        if (montString!!.equals("M")) {
            daysval = "24"
//
        } else if (montString!!.equals("W")) {

            if (eventString!!.equals("Delivery on Saturdays")) {
                daysval = "6"

//                toast(daysval)
            } else {
                daysval = "6"

//                toast(daysval)
            }

        }else if (montString.equals("D")||montString.equals("3")||montString.equals("5")){
            daysval = "3"
        }


        //get Products function
        getProducts()
//        getDays()


        b1.setOnClickListener {
            addtoCart()
//            startActivity(Intent(this, myCart::class.java))
        }

        radio_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
//                Toast.makeText(applicationContext, " On checked change :" + " ${radio.text}", Toast.LENGTH_SHORT).show()
            })
    }


    private fun initRecycler1() {
        rv=findViewById(R.id.choose_extraItem_recycel)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters= choose_extraItem_Adapter(categoryItems, object : choose_extraItem_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this!!)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }

    private fun findViews() {
        radio_group = findViewById(R.id.radioGroup);
        b1 = findViewById(R.id.addExtraItem_next)
        tv = findViewById(R.id.addExtraItem_text)

    }


    fun message(str: String) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show()
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }



    //get Products function
    private fun getProducts() {
        CoroutineScope(Dispatchers.IO).launch {
            val reqval = product_request()
            reqval.productid = "0"
            reqval.subcategory = getStringSharedPref(namesVariable.SUB_CATEGORY,this@choose_extra_item)
            reqval.producttype = "E"
            reqval.vegnonveg = ""
            reqval.duration = ""

            val response = service.getproducts(reqval)
            logd(reqval.toString())
            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            categoryItems.clear()
                            val result = response.body()
                            logd(result.toString())

                            if (result!!.data.size.equals(0)){
                                tv.visibility = View.VISIBLE
                                startActivity(Intent(this@choose_extra_item,myCart::class.java))
                            }

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= choose_extraItem_Adapter(categoryItems, object : choose_extraItem_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val str_id = result.data[position].productid
//                                    toast(str_id)
                                    users.add(str_id)

                                }
                            },this@choose_extra_item!!)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()


//                            Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }




  /*  private fun getDays() {
        CoroutineScope(Dispatchers.IO).launch {
            val reqval = getDates_Request()
            reqval.days = daysval
            reqval.fromdate = getStringSharedPref(namesVariable.DATE_CHOOSED,this@choose_extra_item)
            reqval.schoolid = "0"
            reqval.skipsaturday = skip_sat
            val response = service.getDates(reqval)
            logd(reqval.toString())
            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())
//                            arrayListDateTime.clear()


//                            loadDateData()
                            Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                        }
                    } else {

                        Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }*/


      private fun addtoCart() {
          CoroutineScope(Dispatchers.IO).launch {
              val hours_array = JsonArray()
              val hours_obj2 = JsonObject()
              hours_obj2.addProperty("customerid",getStringSharedPref(namesVariable.USERID, this@choose_extra_item))
              hours_obj2.addProperty("lineid", getStringSharedPref(namesVariable.ADDCART_LID,this@choose_extra_item))
//              hours_obj2.addProperty("lineid","0")
              hours_obj2.addProperty("qty",daysval)
              for (i in 0 until users.size) {
                  val hours_obj1 = JsonObject()
                  hours_obj1.addProperty("productid", users[i])
                  hours_array.add(hours_obj1)
              }
              hours_obj2.add("extraitems",hours_array)

              val response = service.addExtraItemsToCart(hours_obj2)

              logd(response.toString())
              try {
                  withContext(Dispatchers.Main) {
    //                    loading.visibility = View.GONE
                      if (response.body()!!.status == "true") {
                          response.body()?.let {
                              val result = response.body()
                              logd(result.toString())


                              startActivity(Intent(this@choose_extra_item, myCart::class.java))


//                              Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                          }
                      } else {

                          Toast.makeText(this@choose_extra_item, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                      }
                  }
              } catch (e: HttpException) {
                  Log.e("REQUEST", "Exception ${e.message}")
              } catch (e: Throwable) {
                  Log.e("REQUEST", "Ooops: Something else went wrong")
              }
          }

        }
}