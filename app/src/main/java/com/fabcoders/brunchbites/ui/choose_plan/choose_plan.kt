package com.fabcoders.brunchbites.ui.choose_plan

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.Sub_categoryt_product_Adapter
import com.fabcoders.brunchbites.Adapter.choose_plan_Adapter
import com.fabcoders.brunchbites.model.model.getDaily.Data
import com.fabcoders.brunchbites.model.model.getDaily.daily_Request
import com.fabcoders.brunchbites.ui.calendar.calendar
import com.fabcoders.maidan.retrofit.APIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.util.ArrayList

class choose_plan : AppCompatActivity(){

    lateinit var b1 : Button
    lateinit var l1: LinearLayout
    lateinit var l2: LinearLayout
    lateinit var l3: LinearLayout
    lateinit var l4: LinearLayout
    lateinit var l5: LinearLayout

    lateinit var rv : RecyclerView

    lateinit var categoryAdapters: choose_plan_Adapter
    lateinit var categoryItems: ArrayList<Data>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_your_plan)
        findViews()
        setupToolbar()

        categoryItems=ArrayList()
        initRecycler1()

        getData()

//        val newString: String?
//        newString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("STRING_CHOOSE_PLAN")
//        } else {
//            savedInstanceState.getSerializable("STRING_CHOOSE_PLAN") as String?
//        }
//
//        val thaalString: String?
//        thaalString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("THAAL_CHOOSE_PLAN")
//        } else {
//            savedInstanceState.getSerializable("THAAL_CHOOSE_PLAN") as String?
//        }
//        val eventString: String?
//        eventString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("EVENT_CHOOSE_PLAN")
//        } else {
//            savedInstanceState.getSerializable("EVENT_CHOOSE_PLAN") as String?
//        }
//
//        if (newString == "corporate"){
//            l1.setBackgroundResource(R.color.orange_4)
//            l2.setBackgroundResource(R.color.orange_2)
//            l3.setBackgroundResource(R.color.orange_5)
//            l4.setBackgroundResource(R.color.orange_6)
//            l5.setBackgroundResource(R.color.orange_7)
//        }else if(thaalString == "thaal"){
//            l1.setBackgroundResource(R.color.green6)
//            l2.setBackgroundResource(R.color.green3)
//            l3.setBackgroundResource(R.color.green3)
//            l4.setBackgroundResource(R.color.green4)
//            l5.setBackgroundResource(R.color.green1)
//        }else if(eventString == "events"){
//            l1.setBackgroundResource(R.color.purple1)
//            l2.setBackgroundResource(R.color.purple2)
//            l3.setBackgroundResource(R.color.purple4)
//            l4.setBackgroundResource(R.color.purple5)
//            l5.setBackgroundResource(R.color.purple6)
//        }

        b1.setOnClickListener {
//            if (newString == "corporate"){
//                val intent =  Intent(this, choose_plan_days::class.java)
//                val strName: String? = "corporate"
//                intent.putExtra("STRING_CHOOSE_PLAN_DAYS", strName)
//                startActivity(intent)
//            }else if(thaalString == "thaal"){
//                val intent =  Intent(this, choose_plan_days::class.java)
//                val strName2: String? = "thaal"
//                intent.putExtra("THAAL_CHOOSE_PLAN_DAYS", strName2)
//                startActivity(intent)
//            }else if(eventString == "events") {
//                val intent = Intent(this, choose_plan_days::class.java)
//                val strName2: String? = "events"
//                intent.putExtra("EVENTS_CHOOSE_PLAN_DAYS", strName2)
//                startActivity(intent)
//            }else{
//                startActivity(Intent(this,choose_plan_days::class.java))
//            }
//            startActivity(Intent(this,choose_plan_days::class.java))
        }
    }

    private fun initRecycler1() {
        rv=findViewById(R.id.choose_plan_recycler)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters= choose_plan_Adapter(categoryItems, object : choose_plan_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this!!)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }

    private fun getData() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = daily_Request()
            logReq.subcatid = getStringSharedPref(namesVariable.SUB_CATEGORY,this@choose_plan)
            val response = service.chooseDailyorWeekly(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= choose_plan_Adapter(categoryItems, object : choose_plan_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {

                                    val item = result.data[position].duration
                                    setStringSharedPref(namesVariable.DURATION,item,this@choose_plan)

                                    if (item.equals("D")||item.equals("3")||item.equals("5")){

                                        startActivity(Intent(this@choose_plan,calendar::class.java))

                                    }else {
                                        startActivity(Intent(this@choose_plan,choose_plan_days::class.java))
                                    }


                                }
                            },this@choose_plan!!)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@choose_plan, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@choose_plan, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun findViews() {
        b1 = findViewById(R.id.choose_plan_next)
//        l1 = findViewById(R.id.choose_plan_l1)
//        l2 = findViewById(R.id.choose_plan_l2)
//        l3 = findViewById(R.id.choose_plan_l3)
//        l4 = findViewById(R.id.choose_plan_l4)
//        l5 = findViewById(R.id.choose_plan_l5)
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}