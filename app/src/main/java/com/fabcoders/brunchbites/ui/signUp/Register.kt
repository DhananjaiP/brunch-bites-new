package com.fabcoders.brunchbites.ui.signUp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.MainActivity
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.logd
import com.fabcoders.brunchbites.model.model.Regions.registration_Request
import com.fabcoders.maidan.retrofit.APIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class Register : AppCompatActivity() {
    val TAG = "Register"
    lateinit var spinner: Spinner
    val service = APIClient.makeRetrofitService()
    lateinit var selected_category: String
    lateinit var categoryItems: java.util.ArrayList<String>
    lateinit var categoryPos: java.util.ArrayList<String>
    lateinit var register:Button
    lateinit var efullname:EditText
    lateinit var eEmail:EditText
    lateinit var eMobile:EditText
    lateinit var ePassword:EditText
    lateinit var ePassword2:EditText

    var sIds: List<String> = ArrayList()
    var names: List<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewby()
        categoryItems = ArrayList()
        categoryPos = ArrayList()
        getRegions()

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {


//                selected_category = spinner.getItemAtPosition(spinner.selectedItemId.toInt()).toString()

                selected_category = categoryPos[i]
//                Toast.makeText(applicationContext, selected_category, Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                // DO Nothing here
            }
        }

        register.setOnClickListener {
            val name = efullname.text.toString()
            val mobile = eMobile.text.toString()
            val email = eEmail.text.toString()
            val pass2 = ePassword2.text.toString()

            loadData(name,email,mobile,pass2,selected_category)
        }

    }

    private fun viewby() {
        spinner = findViewById(R.id.region_spinner)
        register = findViewById(R.id.register)
        efullname = findViewById(R.id.etName)
        eEmail = findViewById(R.id.etEmail)
        eMobile = findViewById(R.id.etPhone)
        ePassword = findViewById(R.id.etPassword1)
        ePassword2 = findViewById(R.id.etPassword2)
    }

    private fun getRegions() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getRegions()
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()!!

                            logd(result.toString())

                            for (x in 0 until result!!.data.size) {

                                categoryItems.add(result.data[x].regionname)
                                categoryPos.add(result.data[x].regionid)


                            }

                            spinner.adapter = ArrayAdapter<String>(this@Register, android.R.layout.simple_spinner_dropdown_item, categoryItems)


//                            Toast.makeText(this@Register, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@Register, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }



    private fun loadData(name: String, email: String, mobile: String, pass2: String, selectedCategory: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = registration_Request()
            logReq.fullname = name
            logReq.email = email
            logReq.mobile = mobile
            logReq.password = pass2
            logReq.mainregion = selectedCategory
            val response = service.register(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status =="true") {
                        response.body()?.let {
                            val result = response.body()!!
                            logd(result.toString())
                            Toast.makeText(this@Register,"" + response.body()!!.message, Toast.LENGTH_LONG).show()

                            startActivity(Intent(this@Register, MainActivity::class.java))
//
                            finish()

//
                        }
                    } else {

                        Toast.makeText(this@Register,"" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}
