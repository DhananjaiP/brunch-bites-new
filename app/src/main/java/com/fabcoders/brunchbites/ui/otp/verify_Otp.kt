package com.fabcoders.brunchbites.ui.otp


import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.MainActivity
import com.fabcoders.brunchbites.R
import kotlinx.android.synthetic.main.activity_verify_otp.*


class verify_Otp : AppCompatActivity() {

    lateinit var button_verify: Button
    lateinit var otp_et : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_back_nice)
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        findViews()

        button_verify.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
    }

    private fun findViews() {
        button_verify = findViewById(R.id.veifyotp_verify)
        otp_et= findViewById(R.id.verifyotp_et)

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}