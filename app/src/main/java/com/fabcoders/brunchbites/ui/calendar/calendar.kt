package com.fabcoders.brunchbites.ui.calendar

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.daily_dateList_Adapter
import com.fabcoders.brunchbites.Adapter.dateList_Adapter
import com.fabcoders.brunchbites.Adapter.product_Adapter
import com.fabcoders.brunchbites.Home.EventDecorator
import com.fabcoders.brunchbites.Home.HighlightWeekendsDecorator
import com.fabcoders.brunchbites.Model.calendar.Daily_model
import com.fabcoders.brunchbites.Model.calendar.Data
import com.fabcoders.brunchbites.model.model.Holiday.holliday_Request
import com.fabcoders.brunchbites.model.model.Products.product_request
import com.fabcoders.brunchbites.model.model.calendar.getDates_Request
import com.fabcoders.brunchbites.ui.add_Extra_Item.choose_extra_item
import com.fabcoders.brunchbites.ui.myCart.myCart
import com.fabcoders.brunchbites.utility.CustomScrollView
import com.fabcoders.maidan.retrofit.APIClient
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import kotlinx.android.synthetic.main.alerdialog_forgotpass.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDate
import retrofit2.HttpException
import java.text.SimpleDateFormat

class calendar : AppCompatActivity() {


    lateinit var calendarView: MaterialCalendarView
    lateinit var arrayListDateTime: ArrayList<Data>
    lateinit var arrayListDateTime2: ArrayList<Daily_model>
    var arrayListDateTime3: ArrayList<Boolean> = ArrayList()
    lateinit var arrayveg: ArrayList<String>
    lateinit var product_adapter: product_Adapter
    lateinit var productList: ArrayList<com.fabcoders.brunchbites.model.model.Products.Data>
    var dates = ""
    var arrayListname = ArrayList<CalendarDay>()
    lateinit var daysval: String
    lateinit var skip_sat: String
    lateinit var button: Button
    lateinit var rv: RecyclerView
    lateinit var holidayList: ArrayList<String>
    lateinit var categoryAdapters: dateList_Adapter

    lateinit var categoryAdapters2: daily_dateList_Adapter

    val service = APIClient.makeRetrofitService()
    lateinit var sw: Switch
    lateinit var check_veg: String
    lateinit var Product_price_veg: String
    lateinit var Product_price_Nveg: String
    lateinit var ProductID: String

    lateinit var item_date: String
    lateinit var item_vegnonveg: String
    lateinit var chnegRate:String
    lateinit var item_rate: String
    lateinit var item_gstpercent: String

    lateinit var checkk:String
    lateinit var cs : CustomScrollView
    lateinit var caltag:TextView



    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calender)

        sw = findViewById(R.id.choose_cal_vn)
        cs = findViewById(R.id.customScroll_subCategory)
        holidayList = ArrayList()
        arrayListDateTime = ArrayList()
        arrayListDateTime2 = ArrayList()
        productList = ArrayList()
        initRecycler1()
//        initRecycler2()
        calendarView = findViewById(R.id.calendarView)
        caltag = findViewById(R.id.calendar_tag)



        val eventString = getStringSharedPref(namesVariable.DAYS, this)
        val montString = getStringSharedPref(namesVariable.DURATION, this)


        if (montString.equals("D")||montString.equals("3")||montString.equals("5")){
            val instance = LocalDate.now()
            calendarView.state().edit().setMinimumDate(instance).commit()
            calendarView.addDecorator(HighlightWeekendsDecorator())
        }else{
            val instance = LocalDate.now()
            calendarView.setSelectedDate(instance)
            calendarView.state().edit().setMinimumDate(instance).commit()
            calendarView.addDecorator(HighlightWeekendsDecorator())
        }




        calendarView.setOnDateChangedListener(OnDateSelectedListener { widget, date, selected ->
            dates = "${date.day}-${date.month}-${date.year}"
            val parsedDateObj = SimpleDateFormat("dd-M-yyyy").parse(dates)
            dates = SimpleDateFormat("yyyy-MM-dd").format(parsedDateObj)
            setStringSharedPref(namesVariable.DATE_CHOOSED, dates, this)

            if (montString.equals("D")||montString.equals("3")||montString.equals("5") ){
                val true_false:Boolean

                true_false = !setStringSharedPref(namesVariable.VN_SWITCH,check_veg,this).equals("V")


                        arrayListDateTime2.add(Daily_model(dates,true_false))
//                      arrayListDateTime3.add(true_false)
                        loadDateData2()

                categoryAdapters2 = daily_dateList_Adapter(arrayListDateTime2, object : daily_dateList_Adapter.ItemClickListener {
                    override fun clickToIntent(position: Int, checked: Boolean) {

                        arrayListDateTime2[position].veg_nonVeg = checked
                    }
                },
                    this@calendar!!
                )
                rv.adapter = categoryAdapters2
                categoryAdapters2.notifyDataSetChanged()

            }else if(montString != "D"){
                caltag.visibility = View.VISIBLE
//                arrayListDateTime.add(dates)
                getDays()
//
            }
        })


        if (sw.isChecked) {
            check_veg = "N"
            sw.setBackgroundResource(R.color.red)
            setStringSharedPref(namesVariable.VN_SWITCH,check_veg,this)
            //get Products function
            getProducts()
        } else {
            check_veg = "V"
            setStringSharedPref(namesVariable.VN_SWITCH,check_veg,this)
            sw.setBackgroundResource(R.color.green)
            //get Products function
            getProducts()
        }





        button = findViewById(R.id.calendar_next)
        button.setOnClickListener {

//            val bundle = Bundle()
//            bundle.putSerializable("product", arrayListDateTime)
//            val cartIntent = Intent(this, choose_extra_item::class.java)
//            cartIntent.putExtras(bundle)
//            this.startActivity(cartIntent)



            if(getStringSharedPref(namesVariable.CATEGORY_TYPE,this)!=""){
                if (getStringSharedPref(namesVariable.CART_SUBCAT,this)==""||
                    getStringSharedPref(namesVariable.CART_SUBCAT,this)=="School Tiffins"){
                    if(montString.equals("D")||montString.equals("3")||montString.equals("5")){
                        if(!arrayListDateTime2.isEmpty()){
                            addtoCart2()
                        }else{
                            toast("Please select Dates")
                        }

                    }else if(!arrayListDateTime.isEmpty()){
                        addtoCart()
                    }else{
                        toast("Please select Dates")
                    }

                }else{


                    val dialogBuilder = AlertDialog.Builder(this@calendar)
                    val view = LayoutInflater.from(this)
                        .inflate(R.layout.alerdialog_forgotpass, null)
                    dialogBuilder.setView(view)
                    view.forgotpaas_text.setText("School cannot be added with other Menu Item \n please clear Your Cart first.")
                    dialogBuilder.setPositiveButton("ok", DialogInterface.OnClickListener(){
                            dialogInterface, i ->
                        dialogInterface.cancel()
                        startActivity(Intent(this,myCart::class.java))
                    })
                    dialogBuilder.show()
//                    toast("School cannot be added with other Menu Item")
                }
            }else{
                if (getStringSharedPref(namesVariable.CART_SUBCAT,this)=="School Tiffins"){


                    val dialogBuilder = AlertDialog.Builder(this@calendar)
                    val view = LayoutInflater.from(this)
                        .inflate(R.layout.alerdialog_forgotpass, null)
                    dialogBuilder.setView(view)
                    view.forgotpaas_text.setText("Other menu Items cannot be added with School \n please clear your Cart first. ")
                    dialogBuilder.setPositiveButton("ok", DialogInterface.OnClickListener(){
                            dialogInterface, i ->
                        dialogInterface.cancel()
                        startActivity(Intent(this,myCart::class.java))
                    })
                    dialogBuilder.show()

//                    toast("Other menu Items cannot be added with School")

                }else{

                    if(montString.equals("D")||montString.equals("3")||montString.equals("5")){
                        if(!arrayListDateTime2.isEmpty()){
                            addtoCart2()
                        }else{
                            toast("Please select Dates")
                        }

                    }else if(!arrayListDateTime.isEmpty()){
                        addtoCart()
                    }else{
                        toast("Please select Dates")
                    }

                }

            }



//            startActivity(Intent(this@calendar, choose_extra_item::class.java))
        }


//        val c = Calendar.getInstance()
//        val monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH)
//        setStringSharedPref(namesVariable.MONTH_DAYS, monthMaxDays.toString(), this)

//        toast(monthMaxDays.toString())




        if (montString!!.equals("M")) {
            daysval = "24"
//            toast(daysval)
        } else if (montString!!.equals("W")) {

            if (eventString!!.equals("Delivery on Saturdays")) {
                daysval = "6"
                skip_sat = "0"
//                toast(daysval)
            } else {
                daysval = "6"
                skip_sat = "1"
//                toast(daysval)
            }

        }


//        if (eventString!!.equals("6 days/week")) {
//
////            toast(skip_sat)
//        } else {
//
////            toast(skip_sat)
//        }



        sw.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                check_veg = "N"
                //get Products function
                sw.setBackgroundResource(R.color.red)
                setStringSharedPref(namesVariable.VN_SWITCH,check_veg,this@calendar)
                getProducts()
            } else {
                check_veg = "V"
                //get Products function
                sw.setBackgroundResource(R.color.green)
                setStringSharedPref(namesVariable.VN_SWITCH,check_veg,this@calendar)
                getProducts()
            }
        }


        //get holliday function
        getHolidays()



        cs.setOnTouchListener(View.OnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                cs.startScrollerTask()
            }
            false
        })


    }


    private fun initRecycler1() {
        rv = findViewById(R.id.date_listRecycler)
        rv.layoutManager = LinearLayoutManager(this)
//        rv.setHasFixedSize(true)
        rv.isNestedScrollingEnabled = false
        categoryAdapters = dateList_Adapter(arrayListDateTime, object : dateList_Adapter.ItemClickListener {
                override fun clickToIntent(position: Int, checked: Boolean) {
                }
            }, this!!)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }





    //get Products function
    private fun getProducts() {
        CoroutineScope(Dispatchers.IO).launch {
            val reqval = product_request()
            reqval.productid = "0"
            reqval.subcategory = getStringSharedPref(namesVariable.SUB_CATEGORY, this@calendar)
            reqval.producttype = getStringSharedPref(namesVariable.PRODCUTTYPE, this@calendar)
            reqval.vegnonveg = ""
            reqval.duration = getStringSharedPref(namesVariable.DURATION, this@calendar)
            val response = service.getproducts(reqval)
            logd(reqval.toString())
            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())

//                            toast("Product" + result!!.data)

                            Product_price_veg = result!!.data[0].rate
                            Product_price_Nveg= result!!.data[1].rate
                            ProductID = result.data[0].productid
                            setStringSharedPref(namesVariable.PRODUCT_ID,ProductID,this@calendar)




//                            toast("Price" + price)

//                            for (x in result!!.data.indices){
//                                productList.add(result.data[x])
//                            }

//                            product_adapter= product_Adapter(productList, object : product_Adapter.ItemClickListener{
//                                override fun clickToIntent(
//                                    position: Int, checked: Boolean) {
//                                   if (checked)
//                                   {
//                                        price = result.data[position].rate
//                                       toast("price: "+price)
//                                   }
//                                }
//                            },this@calendar!!)
//                            product_adapter.notifyDataSetChanged()


//                            Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    //get Days from calendar
    private fun getDays() {
        CoroutineScope(Dispatchers.IO).launch {
            val reqval = getDates_Request()
            reqval.days = daysval
            reqval.fromdate = dates
            reqval.schoolid = "0"
            reqval.skipsaturday = skip_sat
            val response = service.getDates(reqval)
            logd(reqval.toString())
            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())
                            arrayListDateTime.clear()

                            for (x in result!!.data.indices) {
//                                arrayveg.add("V")
                                arrayListDateTime.add(result.data[x])

                            }

                            loadDateData()

                            categoryAdapters = dateList_Adapter(arrayListDateTime, object : dateList_Adapter.ItemClickListener {
                                    override fun clickToIntent(position: Int, checked: Boolean) {

                                        val ax = checked
//                                        toast(ax.toString())
                                        arrayListDateTime[position].veg_nonVeg = checked
                                    }
                                },
                                this@calendar!!
                            )
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()


//                            Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                        }
                    } else {

                        Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    //get holliday function
    private fun getHolidays() {
        CoroutineScope(Dispatchers.IO).launch {
            val reqval = holliday_Request()
            reqval.schoolid = "0"
            reqval.fromdate = "2020-6-4"
            reqval.todate = "2020-7-1"
            val response = service.getHoliday(reqval)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            for (x in result!!.data.indices) {

                                holidayList.add(result.data[x].holidaydate)

//                                toast(holidayList.toString())

                            }




//                            Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

//
                        }
                    } else {

                        Toast.makeText(
                            this@calendar,
                            "" + response.body()!!.message,
                            Toast.LENGTH_LONG
                        ).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun loadDateData() {
        val calendarView = findViewById<MaterialCalendarView>(R.id.calendarView)
        val instance = LocalDate.now()
        calendarView.setSelectedDate(instance)
        calendarView.state().edit().setMinimumDate(instance).commit()
        calendarView.addDecorator(HighlightWeekendsDecorator())
        if (!arrayListDateTime.isEmpty()) {
            arrayListname.clear()
            calendarView.removeDecorators()
            arrayListDateTime.forEach {
                var date_now = it.date.toString()
                val parsedDateObj = SimpleDateFormat("yyyy-MM-dd").parse(date_now)
                date_now = SimpleDateFormat("dd-M-yyyy").format(parsedDateObj)
                var delimiter = "-"
                val parts = date_now.split(delimiter)
                val day = CalendarDay.from(LocalDate.of(parts[2].toInt(), parts[1].toInt(), parts[0].toInt()))
                arrayListname.add(day)
            }
        }
        calendarView.addDecorator(EventDecorator(Color.GREEN, arrayListname.toMutableList()))

    }



    private fun loadDateData2() {
        val calendarView = findViewById<MaterialCalendarView>(R.id.calendarView)
        val instance = LocalDate.now()
        calendarView.setSelectedDate(instance)
        calendarView.state().edit().setMinimumDate(instance).commit()
        calendarView.addDecorator(HighlightWeekendsDecorator())
        if (!arrayListDateTime2.isEmpty()) {
            arrayListname.clear()
            arrayListDateTime2.forEach {
                var date_now = it.date
                val parsedDateObj = SimpleDateFormat("yyyy-MM-dd").parse(date_now)
                date_now = SimpleDateFormat("dd-M-yyyy").format(parsedDateObj)
                var delimiter = "-"
                val parts = date_now.split(delimiter)
                val day = CalendarDay.from(
                    LocalDate.of(
                        parts[2].toInt(),
                        parts[1].toInt(),
                        parts[0].toInt()
                    )
                )
                arrayListname.add(day)
            }
        }
        calendarView.addDecorator(EventDecorator(Color.GREEN, arrayListname.toMutableList()))

    }


    private fun addtoCart() {
        CoroutineScope(Dispatchers.IO).launch {
//            val reqval = addToCart_Request()
            val hours_array = JsonArray()
            val hours_obj2 = JsonObject()
            hours_obj2.addProperty("customerid",getStringSharedPref(namesVariable.USERID, this@calendar))
            hours_obj2.addProperty("schoolid","0")
            hours_obj2.addProperty("lineid","0")
            hours_obj2.addProperty("subcategoryid",getStringSharedPref(namesVariable.SUB_CATEGORY, this@calendar))
            hours_obj2.addProperty("duration",getStringSharedPref(namesVariable.DURATION, this@calendar))


//            reqval.schoolid = "0"
//            reqval.lineid = "0"
//            reqval.subcategoryid = getStringSharedPref(namesVariable.SUB_CATEGORY, this@calendar)
//            reqval.duration = getStringSharedPref(namesVariable.DURATION, this@calendar)
//            reqval.dates = hours_array.toString()
            for (i in 0 until arrayListDateTime.size) {
//                hours_array.add(arrayListDateTime[i].date[i])
                val xx = arrayListDateTime[i].veg_nonVeg
                val vn:String
                if (xx.equals(true)) {
                    checkk = "N"
                    vn = Product_price_Nveg
                }else{
                    checkk = "V"
                    vn = Product_price_veg
                }
                val hours_obj1 = JsonObject()
                hours_obj1.addProperty("date",arrayListDateTime[i].date)
                hours_obj1.addProperty("productid",ProductID)
                hours_obj1.addProperty("rate",vn)
                hours_obj1.addProperty("vegnonveg",checkk)
                hours_obj1.addProperty("gstpercent","18")

                hours_array.add(hours_obj1)



            }
            hours_obj2.add("dates",hours_array)
//            val hours_array3 = JsonArray()
//            val hours_obj3 = JsonObject()
//
//            hours_obj3.addProperty("productid","")
//            hours_obj3.addProperty("rate","")
//            hours_obj3.addProperty("gstpercent","18")
//
//
//            hours_obj2.add("extraitems",hours_obj3)









//            logd(reqval.toString())

//            toast(dates.toString())

            val response = service.addProductsToCart(hours_obj2)

            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())

                            setStringSharedPref(namesVariable.ADDCART_LID, result!!.lineid.toString(),this@calendar)


                            startActivity(Intent(this@calendar, choose_extra_item::class.java))


//                            Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(
                            this@calendar,
                            "" + response.body()!!.message,
                            Toast.LENGTH_LONG
                        ).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }



    private fun addtoCart2() {
        CoroutineScope(Dispatchers.IO).launch {
//            val reqval = addToCart_Request()
            val hours_array = JsonArray()
            val hours_obj2 = JsonObject()
            hours_obj2.addProperty("customerid",getStringSharedPref(namesVariable.USERID, this@calendar))
            hours_obj2.addProperty("schoolid","0")
            hours_obj2.addProperty("lineid","0")
            hours_obj2.addProperty("subcategoryid",getStringSharedPref(namesVariable.SUB_CATEGORY, this@calendar))
            hours_obj2.addProperty("duration",getStringSharedPref(namesVariable.DURATION, this@calendar))

            for (i in 0 until arrayListDateTime2.size) {
//                hours_array.add(arrayListDateTime2[i])
                val xx = arrayListDateTime2[i].veg_nonVeg
                val vn:String
                if (xx.equals(true)) {
                    checkk = "N"
                    vn = Product_price_Nveg
                }else{
                    checkk = "V"
                    vn = Product_price_veg
                }
                val hours_obj1 = JsonObject()
                hours_obj1.addProperty("date",arrayListDateTime2[i].date)
                hours_obj1.addProperty("productid",ProductID)
                hours_obj1.addProperty("rate",vn)
                hours_obj1.addProperty("vegnonveg",checkk)
                hours_obj1.addProperty("gstpercent","18")

                hours_array.add(hours_obj1)

            }
            hours_obj2.add("dates",hours_array)
            val response = service.addProductsToCart(hours_obj2)

            logd(response.toString())
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()
                            logd(result.toString())

                            setStringSharedPref(namesVariable.ADDCART_LID, result!!.lineid.toString(),this@calendar)


                            startActivity(Intent(this@calendar, choose_extra_item::class.java))


//                            Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@calendar, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }


}