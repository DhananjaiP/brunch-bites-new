package com.fabcoders.brunchbites.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.AddressAdapter
import com.fabcoders.brunchbites.model.model.Address.Add_Edit_address_Request
import com.fabcoders.brunchbites.model.model.Address.getAddress_Request
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class Edit_Address : AppCompatActivity(){

    lateinit var fullAddress:EditText
    lateinit var address_line1:EditText
    lateinit var city:EditText
    lateinit var pincode:EditText
    lateinit var locality:EditText
    lateinit var landmark:EditText




    lateinit var SendAddress:Button

    lateinit var full_add:String
    lateinit var add_line1:String
    lateinit var land:String
    lateinit var cityy:String
    lateinit var localityy:String
    lateinit var pin:String
    lateinit var loading: ProgressBar
    var boolResult = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_edit_address)
        setupToolbar()

        loading = findViewById(R.id.load)
        fullAddress = findViewById(R.id.full_address)
        address_line1 = findViewById(R.id.address_line1)
        landmark = findViewById(R.id.landmark)
        locality = findViewById(R.id.locality)
        city = findViewById(R.id.city)
        pincode = findViewById(R.id.pincode)


        val fulladd: String?
        fulladd = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("FULLADDRESS")
        } else {
            savedInstanceState.getSerializable("FULLADDRESS") as String?
        }
        val add1: String?
        add1 = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("ADDRESS_LINE")
        } else {
            savedInstanceState.getSerializable("ADDRESS_LINE") as String?
        }
        val local: String?
        local = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("LOCALITY")
        } else {
            savedInstanceState.getSerializable("LOCALITY") as String?
        }
        val landd: String?
        landd = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("LAND")
        } else {
            savedInstanceState.getSerializable("LAND") as String?
        }
        val citty: String?
        citty = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("CITY")
        } else {
            savedInstanceState.getSerializable("CITY") as String?
        }
        val pinn: String?
        pinn = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("PINCODE")
        } else {
            savedInstanceState.getSerializable("PINCODE") as String?
        }
        val latt: String?
        latt = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("LAT")
        } else {
            savedInstanceState.getSerializable("LAT") as String?
        }
        val lonn: String?
        lonn = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("LON")
        } else {
            savedInstanceState.getSerializable("LON") as String?
        }

        fullAddress.setText(fulladd)
        address_line1.setText(add1)
        landmark.setText(landd)
        locality.setText(local)
        city.setText(citty)
        pincode.setText(pinn)






        SendAddress = findViewById(R.id.send_address)
        SendAddress.setOnClickListener {
            full_add = fullAddress.text.toString()
            add_line1 = address_line1.text.toString()
            land = landmark.text.toString()
            localityy = locality.text.toString()
            cityy = city.text.toString()
            pin = pincode.text.toString()
            loading.visibility = View.VISIBLE

            editData(full_add,add_line1,land,localityy,cityy,pin)

        }


        val address_id = getStringSharedPref(namesVariable.ADDRESS_ID,this)
        if (address_id.equals("")){
        }else{
            SendAddress.setText("Update")
        }



    }



    private fun editData(
        fullAdd: String,
        addLine1: String,
        land: String,
        localityy: String,
        cityy: String,
        pin: String
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Add_Edit_address_Request()
            logReq.fulladdress = fullAdd
            logReq.address1 = addLine1
            logReq.locality = localityy
            logReq.landmark = land
            logReq.city = cityy
            logReq.pincode = pin
            logReq.lat = "15.4909"
            logReq.lon = "73.8278"
            logReq.userid = getStringSharedPref(namesVariable.USERID,this@Edit_Address)
            logReq.addtype = "H"
            val response = service.EditAddress(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status =="true") {
                        response.body()?.let {
                            val result = response.body()!!
                            logd(response.toString())


//                            startActivity(Intent(this@Edit_Address, MainActivity::class.java))
//                            Toast.makeText(this@Edit_Address,"" + response.body()!!.message, Toast.LENGTH_LONG).show()
//
                            finish()

//
                        }
                    } else {

                        Toast.makeText(this@Edit_Address,"" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }



    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }


//

}
