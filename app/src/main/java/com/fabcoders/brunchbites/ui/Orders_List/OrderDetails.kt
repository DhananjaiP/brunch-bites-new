package com.fabcoders.brunchbites.ui.Orders_List

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.cartProduct_Adapter
import com.fabcoders.brunchbites.Adapter.cart_itemDetail_Adapter
import com.fabcoders.brunchbites.Adapter.categoryt_product_Adapter
import com.fabcoders.brunchbites.Adapter.orderDetail_Adapter
import com.fabcoders.brunchbites.model.model.Categories.DataXX
import com.fabcoders.brunchbites.model.model.Order.Order_Request
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.find
import retrofit2.HttpException

class OrderDetails : AppCompatActivity() {


    lateinit var l1: LinearLayout
    lateinit var l2: LinearLayout
    lateinit var l3: LinearLayout
    lateinit var discount_amount: TextView
    lateinit var discount_percent: TextView
    lateinit var sub_total: TextView
    lateinit var gst: TextView
    lateinit var grand_total: TextView
    lateinit var rvv: RecyclerView
    lateinit var categoryAdapters: orderDetail_Adapter
    lateinit var categoryItems: ArrayList<com.fabcoders.brunchbites.model.model.cart.DataXX>
    lateinit var b1:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_cart)
        setupToolbar()

        categoryItems = ArrayList()
        getViews()
        initRecycler1()

        l1.visibility = View.GONE
        b1.visibility=View.GONE

        getdata()
    }


    private fun initRecycler1() {
        rvv = findViewById(R.id.rv_products)
        rvv.layoutManager = LinearLayoutManager(this)
        rvv.isNestedScrollingEnabled = false
//        rvv.setHasFixedSize(true)
        categoryAdapters = orderDetail_Adapter(categoryItems, object : orderDetail_Adapter.ItemClickListener {


        }, this!!)
        rvv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }

    private fun getViews() {
        l1 = findViewById(R.id.couponCodeLL)
        discount_amount = findViewById(R.id.cart_discount)
        discount_percent = findViewById(R.id.cart_discountPercent)
        sub_total = findViewById(R.id.cart_subtotal)
        gst = findViewById(R.id.cart_gst)
        grand_total = findViewById(R.id.cart_total)
        l2 = findViewById(R.id.cart_discount_lay)
        l3 = findViewById(R.id.cart_discountPercent_lay)
        b1 = findViewById(R.id.mycart_proceed)
    }


    private fun getdata() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Order_Request()
            logReq.orderid = getStringSharedPref(namesVariable.ORDER_ID,this@OrderDetails)
            val response = service.getOrdersDetail(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()

                            if (result!!.discountamount.equals("0.00")) {
                                l2.visibility = View.GONE
                            } else {
                                discount_amount.text = "₹ " + result!!.discountamount
                            }

                            if (result!!.discountpercent.equals("0.00")) {
                                l3.visibility = View.GONE
                            } else {
                                discount_percent.text = "% " + result.discountpercent
                            }



                            sub_total.text = "₹ " + result!!.subtotal
                            gst.text = "₹ " + result.gstamount
                            grand_total.text = "₹ " + result.total

                            for (x in result!!.data.indices) {
                                categoryItems.add(result.data[x])

                            }

                            if (categoryItems.isEmpty()) {

                                discount_amount.text = "₹ 00.0"
                                discount_percent.text = " 00.0" + "%"
                                sub_total.text = "₹ 00.0"
                                gst.text = "₹ 00.0"
                                grand_total.text = "₹ 00.0"
                                setStringSharedPref(namesVariable.GRAND_TOTAL, result.total, this@OrderDetails)

                            }

//                            setStringSharedPref(namesVariable.CART_COUNT, result.data.size.toString(), this@OrderDetails)


                            categoryAdapters = orderDetail_Adapter(categoryItems, object : orderDetail_Adapter.ItemClickListener {



                            },
                                this@OrderDetails!!
                            )
                            rvv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()




//                            Toast.makeText(this@OrderDetails, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@OrderDetails, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}