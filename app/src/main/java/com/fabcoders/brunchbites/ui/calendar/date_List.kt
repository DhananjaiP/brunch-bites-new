package com.fabcoders.brunchbites.ui.calendar

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.Adapter.dateList_Adapter
import com.fabcoders.brunchbites.Model.calendar.Data
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.getStringSharedPref
import com.fabcoders.brunchbites.model.model.calendar.getDates_Request
import com.fabcoders.brunchbites.namesVariable
import com.fabcoders.brunchbites.service
import com.fabcoders.brunchbites.ui.add_Extra_Item.choose_extra_item

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.toast
import retrofit2.HttpException
import java.util.ArrayList

class date_List : AppCompatActivity() {

    lateinit var button : Button
    lateinit var daysval: String
    lateinit var skip_sat: String
    lateinit var dates: String
    lateinit var rv : RecyclerView
    lateinit var categoryItems: ArrayList<Data>
    lateinit var categoryAdapters: dateList_Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calendar_date_list)

        categoryItems=ArrayList()
        initRecycler1()

        getDays()

        button = findViewById(R.id.dateList_next)
        button.setOnClickListener {
            startActivity(Intent(this, choose_extra_item::class.java))
        }



        val eventString = getStringSharedPref(namesVariable.DAYS, this)
        val montString = getStringSharedPref(namesVariable.DURATION, this)
        val monthMaxDays = getStringSharedPref(namesVariable.MONTH_DAYS, this)
        dates = getStringSharedPref(namesVariable.DATE_CHOOSED, this)

        if (montString!!.equals("M")) {
            daysval = monthMaxDays.toString()
            toast(daysval)
        } else if (montString!!.equals("W")) {

            if (eventString!!.equals("6 days/week")) {
                daysval = "6"
                toast(daysval)
            } else {
                daysval = "5"
                toast(daysval)
            }

        }

        if (eventString!!.equals("6 days/week")) {
            skip_sat = "0"
            toast(skip_sat)
        } else {
            skip_sat = "1"
            toast(skip_sat)
        }


    }

    private fun getDays() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = getDates_Request()
            logReq.days = daysval
            logReq.fromdate = dates
            logReq.schoolid= "0"
            logReq.skipsaturday = skip_sat
            val response = service.getDates(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= dateList_Adapter(categoryItems, object : dateList_Adapter.ItemClickListener{
                                override fun clickToIntent(
                                    position: Int,
                                    checked: Boolean
                                ) {


                                }
                            },this@date_List!!)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

                            Toast.makeText(this@date_List, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@date_List, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun initRecycler1() {
        rv=findViewById(R.id.date_listRecycler)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters= dateList_Adapter(categoryItems, object : dateList_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int, checked: Boolean) {
            }
        }, this!!)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }




}