package com.fabcoders.brunchbites.ui.Profile

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.getStringSharedPref
import com.fabcoders.brunchbites.namesVariable

class Profile : AppCompatActivity() {

    lateinit var name:TextView
    lateinit var email:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)
        setupToolbar()
        findViews()


        name.setText(getStringSharedPref(namesVariable.PERSON_NAME,this))
        email.setText(getStringSharedPref(namesVariable.EMAIL,this))


    }

    private fun findViews() {
        name = findViewById(R.id.personName)
        email = findViewById(R.id.personEmail)
    }


    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}