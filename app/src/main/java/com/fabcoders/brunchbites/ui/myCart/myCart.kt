package com.fabcoders.brunchbites.ui.myCart

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.cartProduct_Adapter
import com.fabcoders.brunchbites.Adapter.cart_Extra_itemDetail_Adapter
import com.fabcoders.brunchbites.Adapter.cart_itemDetail_Adapter
import com.fabcoders.brunchbites.fragment.AddressListFragment
import com.fabcoders.brunchbites.model.model.cart.Cart_Request
import com.fabcoders.brunchbites.model.model.cart.Data
import com.fabcoders.brunchbites.model.model.cart.Date
import com.fabcoders.brunchbites.model.model.cart.Extraitem
import com.fabcoders.brunchbites.utility.CustomScrollView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import kotlin.collections.ArrayList

class myCart : AppCompatActivity() {

    lateinit var detail: Button
    lateinit var proceed: Button
    lateinit var coupon_text: EditText
    lateinit var apply_coupon: Button
    lateinit var discount_amount: TextView
    lateinit var discount_percent: TextView
    lateinit var sub_total: TextView
    lateinit var gst: TextView
    lateinit var grand_total: TextView
    lateinit var cs : CustomScrollView


    lateinit var itemAdapters: cart_itemDetail_Adapter
    lateinit var detailItems: ArrayList<Date>
    lateinit var ExtraitemAdapters: cart_Extra_itemDetail_Adapter
    lateinit var extradetailItems: ArrayList<Extraitem>

    lateinit var categoryAdapters: cartProduct_Adapter
    lateinit var categoryItems: ArrayList<Data>
    lateinit var rvv: RecyclerView

    lateinit var rv2: RecyclerView
    lateinit var extaItemrv: RecyclerView

    lateinit var l1: LinearLayout
    lateinit var l2: LinearLayout
    lateinit var removeCoupon: Button

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_cart)
        setupToolbar()
        getViews()
        detailItems= ArrayList()
        extradetailItems= ArrayList()
        categoryItems = ArrayList()
        initRecycler1()
//        initRecycler2()

        apply_coupon.setOnClickListener {
            val cpn = coupon_text.text.toString()
            applyCoupon(cpn)
        }

        getcart()

        if (getStringSharedPref(namesVariable.COUPONCODE,this@myCart).equals("")){
            apply_coupon.visibility = View.VISIBLE
        }else{
            coupon_text.setText(getStringSharedPref(namesVariable.COUPONCODE,this))
            apply_coupon.visibility = View.GONE
            removeCoupon.visibility = View.VISIBLE
        }

        removeCoupon.setOnClickListener {
            removeCoupon()
            removeCoupon.visibility = View.GONE
        }

        cs.setOnTouchListener(View.OnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                cs.startScrollerTask()
            }
            false
        })


        proceed.setOnClickListener {
            val flaag = "1"
            setStringSharedPref(namesVariable.FLAAG, flaag, this)
            startActivity(Intent(this, AddressListFragment::class.java))
        }

//        detail.setOnClickListener {
//            val dialogBuilder = AlertDialog.Builder(this)
//            val view = LayoutInflater.from(this).inflate(R.layout.alertdialog_mycart_details, null)
//            dialogBuilder.setView(view)
//            val alert = dialogBuilder.create()
//            alert.show()
//        }
    }

    private fun initRecycler1() {
        rvv = findViewById(R.id.rv_products)
        rvv.layoutManager = LinearLayoutManager(this)
        rvv.isNestedScrollingEnabled = false
//        rvv.setHasFixedSize(true)
        categoryAdapters = cartProduct_Adapter(categoryItems, object : cartProduct_Adapter.ItemClickListener {
                override fun clickToIntent(position: Int) {
                }

                override fun clickToIntent1(position: Int) {
                    TODO("Not yet implemented")
                }
            }, this!!)
        rvv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }

    private fun applyCoupon(cpn: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.userid = getStringSharedPref(namesVariable.USERID, this@myCart)
            logReq.vouchercode = cpn
            val response = service.applyCoupon(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()

                            getcart()
                            setStringSharedPref(namesVariable.COUPONCODE,cpn,this@myCart)
                            apply_coupon.visibility = View.GONE
                            removeCoupon.visibility = View.VISIBLE
                            Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }



    private fun removeCoupon() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.userid = getStringSharedPref(namesVariable.USERID, this@myCart)
            val response = service.removCoupon(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()

                            getcart()
                            apply_coupon.visibility = View.VISIBLE
                            setStringSharedPref(namesVariable.COUPONCODE,"",this@myCart)
                            coupon_text.setText("+ Enter Promo Code")

//                            Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                        }
                    } else {

                        Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun onDelete(linID: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID, this@myCart)
            logReq.lineid = linID
            val response = service.Delete_cart_Item(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()

                            getcart()


//                            Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

//
                        }
                    } else {

                        Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun getcart() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID, this@myCart)
            val response = service.getCart(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            categoryItems.clear()
                            val result = response.body()


                            if (result!!.discountamount.equals("0.00")) {
                                l1.visibility = View.GONE
                            } else {
                                l1.visibility = View.VISIBLE
                                discount_amount.text = "- ₹ " + result!!.discountamount
                            }

                            if (result!!.discountpercent.equals("0.00")) {
                                l2.visibility = View.GONE
                            } else {
                                l2.visibility = View.VISIBLE
                                discount_percent.text = "% " + result.discountpercent
                            }



                            sub_total.text = "₹ " + result!!.subtotal
                            gst.text = "₹ " + result.gstamount
                            grand_total.text = "₹ " + result.total
                            setStringSharedPref(namesVariable.GRAND_TOTAL, result.total, this@myCart)




                            for (x in result!!.data.indices) {
                                categoryItems.add(result.data[x])

                            }

                            if (categoryItems.isEmpty()) {

//                                discount_amount.text = "- ₹ 00.0"
//                                discount_percent.text = " 00.0" + "%"

                                l1.visibility = View.GONE
                                l2.visibility = View.GONE
                                sub_total.text = "₹ 00.0"
                                gst.text = "₹ 00.0"
                                grand_total.text = "₹ 00.0"
                                setStringSharedPref(namesVariable.GRAND_TOTAL, result.total, this@myCart)

                            }

                            if(result.data.isEmpty()){
                                setStringSharedPref(namesVariable.CART_COUNT, "0", this@myCart)
                                setStringSharedPref(namesVariable.CART_SUBCAT,"",this@myCart)
                            }
                            setStringSharedPref(namesVariable.CART_COUNT, result.data.size.toString(), this@myCart)


                            categoryAdapters = cartProduct_Adapter(categoryItems, object : cartProduct_Adapter.ItemClickListener {
                                    override fun clickToIntent(position: Int) {
                                        val linID = result.data[position].lineid
                                        onDelete(linID)
                                    }

                                    override fun clickToIntent1(position: Int) {

                                        setStringSharedPref(namesVariable.LID,result.data[position].lineid,this@myCart)
                                        val linID = result.data[position].lineid

                                        startActivity(Intent(this@myCart,cart_item_detail::class.java))

//                                        val dialogBuilder = AlertDialog.Builder(this@myCart)
//                                        val view = LayoutInflater.from(this@myCart).inflate(R.layout.alertdialog_mycart_details, null)
//                                        dialogBuilder.setView(view)
//                                        val alert = dialogBuilder.create()
//                                        rv2 = view.findViewById(R.id.cartItemRecycler)
//                                        rv2.layoutManager = LinearLayoutManager(this@myCart)
//                                        rv2.setHasFixedSize(false)
//                                        itemAdapters = cart_itemDetail_Adapter(detailItems, object : cart_itemDetail_Adapter.ItemClickListener {}, this@myCart!!)
//                                        rv2.adapter = itemAdapters
//                                        itemAdapters.notifyDataSetChanged()
//
//                                        extaItemrv = view.findViewById(R.id.ExtraItemRecycler)
//                                        extaItemrv.layoutManager = LinearLayoutManager(this@myCart)
//                                        extaItemrv.setHasFixedSize(true)
//                                        ExtraitemAdapters = cart_Extra_itemDetail_Adapter(extradetailItems, object : cart_Extra_itemDetail_Adapter.ItemClickListener {}, this@myCart!!)
//                                        extaItemrv.adapter = ExtraitemAdapters
//                                        ExtraitemAdapters.notifyDataSetChanged()
//
//
//                                        getitemdata(linID)
//                                        alert.show()

                                        }


                                },
                                this@myCart!!
                            )
                            rvv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()


//                            Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

//
                        }
                    } else {

                        Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun initRecycler2() {
        rv2 = findViewById(R.id.rv_products)
        rv2.layoutManager = LinearLayoutManager(this)
//        rvv.isNestedScrollingEnabled = false
//        rvv.setHasFixedSize(true)
        itemAdapters = cart_itemDetail_Adapter(detailItems, object : cart_itemDetail_Adapter.ItemClickListener {

        }, this!!)
        rvv.adapter = itemAdapters
        itemAdapters.notifyDataSetChanged()

    }

//    private fun getitemdata(linID: String) {
//        CoroutineScope(Dispatchers.IO).launch {
//            val logReq = Cart_Request()
//            logReq.lineid =linID
//            val response = service.cartItemDetail(logReq)
//            try {
//                withContext(Dispatchers.Main) {
////                    loading.visibility = View.GONE
//                    detailItems.clear()
//                    if (response.body()!!.status == "true") {
//                        response.body()?.let {
//
//
//                            val result = response.body()
//                            result!!.extraitems.size.equals(null)
//
//
//                            for (x in result!!.dates.indices){
//                                detailItems.add(result.dates[x])
//                                extradetailItems.add(result.extraitems[x])
//
//                            }
//
//                            itemAdapters= cart_itemDetail_Adapter(detailItems, object : cart_itemDetail_Adapter.ItemClickListener{
//
//
//                            },this@myCart!!)
//                            rv2.adapter = itemAdapters
//                            itemAdapters.notifyDataSetChanged()
//
//
//                            ExtraitemAdapters= cart_Extra_itemDetail_Adapter(extradetailItems, object : cart_Extra_itemDetail_Adapter.ItemClickListener{
//
//
//                            },this@myCart!!)
//                            extaItemrv.adapter = ExtraitemAdapters
//                            ExtraitemAdapters.notifyDataSetChanged()
//
//                            Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
//
//
////
//                        }
//                    } else {
//
//                        Toast.makeText(this@myCart, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
//
//                    }
//                }
//            } catch (e: HttpException) {
//                Log.e("REQUEST", "Exception ${e.message}")
//            } catch (e: Throwable) {
//                Log.e("REQUEST", "Ooops: Something else went wrong")
//            }
//        }
//
//    }

    private fun getViews() {
        proceed = findViewById(R.id.mycart_proceed)
        apply_coupon = findViewById(R.id.couponCodeButton)
        coupon_text = findViewById(R.id.couponCode_text)
        cs = findViewById(R.id.customScroll_subCategory)
        discount_amount = findViewById(R.id.cart_discount)
        discount_percent = findViewById(R.id.cart_discountPercent)
        sub_total = findViewById(R.id.cart_subtotal)
        gst = findViewById(R.id.cart_gst)
        grand_total = findViewById(R.id.cart_total)
        l1 = findViewById(R.id.cart_discount_lay)
        l2 = findViewById(R.id.cart_discountPercent_lay)
        removeCoupon = findViewById(R.id.removeCodeButton)
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)
        toolbar.setNavigationOnClickListener {
            val intent = Intent(this, com.fabcoders.brunchbites.MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }


    override fun onBackPressed() {
//        super.onBackPressed()
        finish()
        val intent = Intent(this, com.fabcoders.brunchbites.MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        //        overridePendingTransition(R.anim.enter, R.anim.exit)
        super.onBackPressed()
    }
}