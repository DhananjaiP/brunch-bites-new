package com.fabcoders.brunchbites.ui.choose_school

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.area_Adapter
import com.fabcoders.brunchbites.Adapter.school_Adapter
import com.fabcoders.brunchbites.Model.Areas.Data
import com.fabcoders.brunchbites.model.model.Areas.areaRequest
import com.fabcoders.brunchbites.model.model.schoool_List.schoolList_Request
import com.fabcoders.brunchbites.ui.menu.Menu

import com.fabcoders.maidan.retrofit.APIClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.util.ArrayList

class choose_school : AppCompatActivity(){

    lateinit var b1 : Button
    lateinit var t1 : TextView
    lateinit var l1: LinearLayout
    lateinit var l2:LinearLayout
    lateinit var l3:LinearLayout
    lateinit var categoryAdapters: area_Adapter
    lateinit var schoolAdapters: school_Adapter
    lateinit var categoryItems: ArrayList<Data>
    val service = APIClient.makeRetrofitService()
    lateinit var categoryItems2: ArrayList<com.fabcoders.brunchbites.Model.schoool_List.Data>
    lateinit var rv : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_a_school)
        setupToolbar()
        findViews()
        categoryItems=ArrayList()
        categoryItems2=ArrayList()
        initRecycler1()
        initRecycler2()

        if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
            getAreas()
        }else{
            getSchools()
        }



//        val newString: String?
//        newString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("STRING_CHOOSE_SCHOOL")
//        } else {
//            savedInstanceState.getSerializable("STRING_CHOOSE_SCHOOL") as String?
//        }
//
//        val thaalString: String?
//        thaalString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("THAAL_CHOOSE_SCHOOL")
//        } else {
//            savedInstanceState.getSerializable("THAAL_CHOOSE_SCHOOL") as String?
//        }
//
//        val eventString: String?
//        eventString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("EVENTS_CHOOSE_SCHOOL")
//        } else {
//            savedInstanceState.getSerializable("EVENTS_CHOOSE_SCHOOL") as String?
//        }
//
//        if (newString == "corporate"){
//            t1.text = "Choose Delivery Area"
//            l1.setBackgroundResource(R.color.orange_1)
//            l2.setBackgroundResource(R.color.orange_2)
//            l3.setBackgroundResource(R.color.orange_3)
//        }else if(thaalString == "thaal"){
//            t1.text = "Choose Delivery Area"
//            l1.setBackgroundResource(R.color.green2)
//            l2.setBackgroundResource(R.color.green3)
//            l3.setBackgroundResource(R.color.green4)
//        } else if(eventString == "events"){
//            t1.text = "Choose Delivery Area"
//            l1.setBackgroundResource(R.color.purple1)
//            l2.setBackgroundResource(R.color.purple2)
//            l3.setBackgroundResource(R.color.purple3)
//        }

        b1.setOnClickListener {
//            if (newString == "corporate"){
//                val intent =  Intent(this, choose_plan::class.java)
//                val strName: String? = "corporate"
//                intent.putExtra("STRING_CHOOSE_PLAN", strName)
//                startActivity(intent)
//            }else if(thaalString == "thaal"){
//                val intent =  Intent(this, choose_plan::class.java)
//                val strName2: String? = "thaal"
//                intent.putExtra("THAAL_CHOOSE_PLAN", strName2)
//                startActivity(intent)
//            }else if(eventString == "events"){
//                val intent =  Intent(this, choose_plan::class.java)
//                val strName3: String? = "events"
//                intent.putExtra("EVENT_CHOOSE_PLAN", strName3)
//                startActivity(intent)
//            }else{
//                startActivity(Intent(this,choose_plan::class.java))
//            }
            startActivity(Intent(this,Menu::class.java))
        }
    }




    private fun initRecycler1() {
        rv=findViewById(R.id.recycel_chooseSchool)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters= area_Adapter(categoryItems, object : area_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()
    }


    private fun initRecycler2() {
        rv=findViewById(R.id.recycel_chooseSchool)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        schoolAdapters= school_Adapter(categoryItems2, object : school_Adapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }
        }, this)
        rv.adapter = schoolAdapters
        schoolAdapters.notifyDataSetChanged()
    }

    private fun getAreas() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = areaRequest()
            logReq.regionid = getStringSharedPref(namesVariable.REGION_ID,this@choose_school)

            val response = service.getAreas(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= area_Adapter(categoryItems, object : area_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val pick_str = result.data[position].areaid
                                    setStringSharedPref(namesVariable.CATEGORY,pick_str,this@choose_school)
//                                    toast(pick_str)

//                                    startActivity(Intent(this@choose_school,Menu::class.java))

//                                    if (result.data[position].cattype.equals("")){
//                                        startActivity(Intent(this@MainActivity, Menu::class.java))
//                                    }else{
//                                        startActivity(Intent(this@MainActivity,choose_school::class.java))
//                                    }

                                }
                            }, this@choose_school)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@choose_school, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@choose_school, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun getSchools() {

        CoroutineScope(Dispatchers.IO).launch {
            val logReq1 = schoolList_Request()
            logReq1.regionid = getStringSharedPref(namesVariable.REGION_ID,this@choose_school)
            logReq1.areaid = "8"
            logReq1.sbtype =getStringSharedPref(namesVariable.CATEGORY_TYPE,this@choose_school)

            val response = service.getSchoolAreas(logReq1)
                    try {
                        withContext(Dispatchers.Main) {
//                      loading.visibility = View.GONE
                            if (response.body()!!.status == "true") {
                                response.body()?.let {

                                    categoryItems2.clear()
                                    val result = response.body()

                                    for (x in result!!.data.indices){
                                        categoryItems2.add(result.data[x])
                                    }


                            schoolAdapters= school_Adapter(categoryItems2, object : school_Adapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    val pick_id = result.data[position].sbid
                                    val pick_str = result.data[position].sbname
//                                    setStringSharedPref(namesVariable.CATEGORY,pick_str,this@choose_school)
//                                    toast(pick_str)

                                    if (pick_id.equals("")){

                                    }else{

                                    }

//                                    if (result.data[position].cattype.equals("")){
//                                        startActivity(Intent(this@MainActivity, Menu::class.java))
//                                    }else{
//                                        startActivity(Intent(this@MainActivity,choose_school::class.java))
//                                    }

                                }
                            }, this@choose_school)
                            rv.adapter = schoolAdapters
                            schoolAdapters.notifyDataSetChanged()

//                            Toast.makeText(this@choose_school, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@choose_school, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    private fun findViews() {
        b1 = findViewById(R.id.choose_school_next)
        t1 = findViewById(R.id.choose_school_title)
        l1 = findViewById(R.id.choose_school_l1)
        l2 = findViewById(R.id.choose_school_l2)
        l3 = findViewById(R.id.choose_school_l3)
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}