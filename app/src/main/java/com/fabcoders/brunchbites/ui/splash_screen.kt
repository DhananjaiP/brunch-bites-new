package com.fabcoders.brunchbites.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.ui.login.LogIn
import com.fabcoders.brunchbites.R

class splash_screen : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.splash)
        Handler().postDelayed({

//            val appPrefs = ApplicationPreferences(this)
//            if (appPrefs.isLoggedIn() && appPrefs.isPhoneSet()) {
//                startActivity(Intent(this, if (appPrefs.isIntroShown()) MainActivity::class.java else IntroActivity::class.java))
//                overridePendingTransition(0,0)
//            } else if (appPrefs.isRegistered()) {
//                val intent = Intent(this, VerifyUserPhone::class.java)
//                startActivity(intent)
//            } else {
            startActivity(Intent(this, LogIn::class.java))
//            }
            finish()
        }, 2000)
    }
}