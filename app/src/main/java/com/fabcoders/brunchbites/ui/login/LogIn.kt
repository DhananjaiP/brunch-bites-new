package com.fabcoders.brunchbites.ui.login

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.fragment.AddressListFragment
import com.fabcoders.brunchbites.model.model.login.loginRequest
import com.fabcoders.brunchbites.ui.signUp.Register
import com.fabcoders.brunchbites.ui.otp.send_Otp
import com.fabcoders.maidan.retrofit.APIClient
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.alerdialog_forgotpass.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class LogIn : AppCompatActivity(), View.OnClickListener {

    lateinit var login: Button
    lateinit var user_Id: String
    lateinit var user_pass: String
    var boolResult = true
    val service = APIClient.makeRetrofitService()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        val username = findViewById<EditText>(R.id.basic_detail_mn)
        val password = findViewById<EditText>(R.id.basic_detail_pass)
        val login = findViewById<Button>(R.id.login)
        findviewid()

        signup_toggle.setOnClickListener(this)
        forgot.setOnClickListener(this)
        forgot.text = Html.fromHtml(getString(R.string.forgot_password))
        signup_toggle.text = Html.fromHtml(getString(R.string.new_user))




        if (hasInternetConnected(this)) {
            if (boolResult) {
//                loading!!.visibility = View.VISIBLE
                checkLogin()
            } else {

//                loading!!.visibility = View.GONE
            }
        } else {
//            this.toast(getString(R.string.internetNA))
//            loading!!.visibility = View.GONE
        }

        login.setOnClickListener {
            user_Id = username.text.toString()
            user_pass = password.text.toString()
//            loading.visibility = View.VISIBLE


            if (hasInternetConnected(this)) {
                if (boolResult) {
//                    loading!!.visibility = View.VISIBLE
                    loadData(user_Id, user_pass)
                } else {
//                    loading!!.visibility = View.GONE


                }
            } else {
                this.toast("No Internet Connection")
//                loading!!.visibility = View.GONE
            }

        }
    }

    private fun findviewid() {
        login = findViewById(R.id.login)

    }

    override fun onClick(p0: View?) {
        val id = p0?.id
        when (id) {
            R.id.signup_toggle -> {
                val intent = Intent(this, Register::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                finish()
            }
            R.id.forgot -> {
                forgotpass()
//                val intent = Intent(this, send_Otp::class.java)
////                intent.putExtra(NAVIGATE_TO, CHANGE_PASSWORD)
//                startActivity(intent)
            }
        }
    }

    private fun forgotpass() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = loginRequest()
            logReq.email = getStringSharedPref(namesVariable.EMAIL, this@LogIn)

            val response = service.forgotpass(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()!!
                            logd(response.toString())

                            val dialogBuilder = AlertDialog.Builder(this@LogIn)
                            val view = LayoutInflater.from(this@LogIn)
                                .inflate(R.layout.alerdialog_forgotpass, null)
                            dialogBuilder.setView(view)
                            view.forgotpaas_text.setText(result.message)
                            dialogBuilder.setPositiveButton("ok",DialogInterface.OnClickListener(){
                                dialogInterface, i ->
                                dialogInterface.cancel()
                            })
                            dialogBuilder.show()

//                            Toast.makeText(this@LogIn, result.message, Toast.LENGTH_LONG).show()

//
                        }
                    } else {

                        Toast.makeText(
                            this@LogIn,
                            "" + response.body()!!.message,
                            Toast.LENGTH_LONG
                        ).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun checkLogin() {
        if (!getStringSharedPref(
                namesVariable.USERINPUTID,
                this
            ).equals("") && !getStringSharedPref(namesVariable.PASSWORD, this).equals("")
        ) {
//            toast(getString(R.string.please_wait))
//            loading.visibility = View.VISIBLE
//            loadData(getStringSharedPref(namesVariable.PHONE_NUMBER, this), getStringSharedPref(namesVariable.PASSWORD, this))

//            startActivity(Intent(this@LogIn, AddressListFragment::class.java))
            startActivity(Intent(this@LogIn, MainActivity::class.java))

            finish()
        } else {
//            loading.visibility = View.GONE

        }
    }

    private fun loadData(userId: String, userPass: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = loginRequest()
            logReq.mobile = userId
            logReq.password = userPass
            val response = service.getLogin(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {
                            val result = response.body()!!
                            logd(response.toString())
                            setStringSharedPref(
                                namesVariable.USERINPUTID,
                                result.userid,
                                this@LogIn
                            )
                            setStringSharedPref(
                                namesVariable.PHONE_NUMBER,
                                result.mobile,
                                this@LogIn
                            )
                            setStringSharedPref(namesVariable.PASSWORD, user_pass, this@LogIn)
                            setStringSharedPref(
                                namesVariable.USERID,
                                response.body()!!.userid,
                                this@LogIn
                            )
                            setStringSharedPref(
                                namesVariable.REGION_ID,
                                response.body()!!.mainregion,
                                this@LogIn
                            )
                            setStringSharedPref(
                                namesVariable.PERSON_NAME,
                                response.body()!!.fullname,
                                this@LogIn
                            )
                            setStringSharedPref(
                                namesVariable.EMAIL,
                                response.body()!!.email,
                                this@LogIn
                            )



                            startActivity(Intent(this@LogIn, MainActivity::class.java))
                            Toast.makeText(
                                this@LogIn,
                                "" + response.body()!!.message,
                                Toast.LENGTH_LONG
                            ).show()
//
                            finish()

//
                        }
                    } else {

                        Toast.makeText(
                            this@LogIn,
                            "" + response.body()!!.message,
                            Toast.LENGTH_LONG
                        ).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


}
