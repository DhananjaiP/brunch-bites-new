package com.fabcoders.brunchbites.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.AddressAdapter
import com.fabcoders.brunchbites.Adapter.getstudentAdapter
import com.fabcoders.brunchbites.Model.Address.Data
import com.fabcoders.brunchbites.model.model.Address.getAddress_Request
import com.fabcoders.brunchbites.model.model.Student.Add_student_tocart_Request
import com.fabcoders.brunchbites.model.model.Student.getStudent_Request
import com.fabcoders.brunchbites.model.model.cart.Cart_Request
import com.fabcoders.brunchbites.ui.Payment.Pay
import com.fabcoders.brunchbites.ui.Student.Edit_Student
import com.fabcoders.brunchbites.ui.Student.add_Student
import com.fabcoders.brunchbites.ui.myCart.myCart
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.util.ArrayList

class AddressListFragment : AppCompatActivity(){
    lateinit var rv : RecyclerView
    lateinit var categoryAdapters: AddressAdapter
    lateinit var categoryItems: ArrayList<Data>
    lateinit var nextButton:Button
    lateinit var categoryAdapters2: getstudentAdapter
    lateinit var categoryItems2: ArrayList<com.fabcoders.brunchbites.model.model.Student.Data>
    lateinit var addb1: TextView
    lateinit var addb2:Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragement_address_list)
        setupToolbar()

        findViews()

        addb1.setOnClickListener {

            if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
                startActivity(Intent(this,Add_Address::class.java))
            }else{
                startActivity(Intent(this,add_Student::class.java))
            }

        }

        addb2.setOnClickListener {

            if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
                startActivity(Intent(this,Add_Address::class.java))
            }else{
                startActivity(Intent(this,add_Student::class.java))
            }

        }


        val fg = getStringSharedPref(namesVariable.FLAAG,this@AddressListFragment)
        if(fg.equals("1")){
            nextButton.visibility = View.VISIBLE
        }

        nextButton.setOnClickListener {
            setStringSharedPref(namesVariable.FLAAG, "0", this)
            if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
                 sendAddressData()
            }else{
                sendStudentData()
            }

        }

        categoryItems=ArrayList()
        categoryItems2=ArrayList()
        initRecycler1()



            if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
                initRecycler1()
            }else{
                initRecycler2()
            }



        
        
        


        if (getStringSharedPref(namesVariable.CATEGORY_TYPE,this)==""){
            getMyAddressesList()
        }else{
            addb1.setText("Add Student")
            addb2.setText("Add new Student")
            getStudentsList()
        }




    }

    private fun sendStudentData() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Add_student_tocart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID,this@AddressListFragment)
            logReq.studentid = getStringSharedPref(namesVariable.EDIT_STUDENT_ID,this@AddressListFragment)

            val response = service.addStudentToCart(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

//                            categoryItems2.clear()
                            val result = response.body()

//                            val alertDialog : AlertDialog.Builder = AlertDialog.Builder(this@AddressListFragment,R.layout.alertdialog_cod)
//                            alertDialog.setTitle("Payment Method")
//                                .setMessage("Only Cash on Delivery option is available. Would you like to proceed further?")
//                                .setPositiveButton(android.R.string.ok){ dialogInterface: DialogInterface, i: Int ->
//                                    Toast.makeText(this@AddressListFragment,"Proceeding..",Toast.LENGTH_LONG).show()
//                                    placeOrder()
//                                }
//                                .setNegativeButton(android.R.string.cancel){ dialogInterface: DialogInterface, i: Int ->
//                                    Toast.makeText(this@AddressListFragment,"cancelled",Toast.LENGTH_LONG).show()
//                                }
//                                .show()


                            startActivity(Intent(this@AddressListFragment,Pay::class.java))
//
//

//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun sendAddressData() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID,this@AddressListFragment)
            logReq.addressid = getStringSharedPref(namesVariable.ADDRESS_ID,this@AddressListFragment)

            val response = service.addAddressToCart(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {


                            val result = response.body()
//
                            startActivity(Intent(this@AddressListFragment,Pay::class.java))
//                            val alertDialog : AlertDialog.Builder = AlertDialog.Builder(this@AddressListFragment,R.layout.alertdialog_cod)
//                            alertDialog.setTitle("Payment Method")
//                                .setMessage("Only Cash on Delivery option is available. Would you like to proceed further?")
//                                .setPositiveButton(android.R.string.ok){ dialogInterface: DialogInterface, i: Int ->
//                                    Toast.makeText(this@AddressListFragment,"Proceeding..",Toast.LENGTH_LONG).show()
//                                    placeOrder()
//                                }
//                                .setNegativeButton(android.R.string.cancel){ dialogInterface: DialogInterface, i: Int ->
//                                    Toast.makeText(this@AddressListFragment,"cancelled",Toast.LENGTH_LONG).show()
//                                }
//                                .show()
//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun placeOrder() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID,this@AddressListFragment)

            val response = service.ordernow(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()
//                            toast(result!!.message)
                            startActivity(Intent(this@AddressListFragment,myCart::class.java))
//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun getStudentsList() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = getStudent_Request()
            logReq.userid = getStringSharedPref(namesVariable.USERID,this@AddressListFragment)

            val response = service.getStudents(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems2.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems2.add(result.data[x])
                            }

                            categoryAdapters2= getstudentAdapter(categoryItems2, object : getstudentAdapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {

                                    val deleteId = result.data[position].studentid
                                    deleteStudent(deleteId)
                                }

                                override fun clickToIntent2(position2: Int) {
                                    setStringSharedPref(namesVariable.EDIT_STUDENT_ID,result.data[position2].studentid,this@AddressListFragment)
                                    setStringSharedPref(namesVariable.STUDENT_NAME,result.data[position2].studentname,this@AddressListFragment)
                                    setStringSharedPref(namesVariable.STUDENT_CLASS,result.data[position2].classdetail,this@AddressListFragment)
                                    startActivity(Intent(this@AddressListFragment,Edit_Student::class.java))
                                }

                                override fun clickToIntent3(position: Int) {
                                   val id =  result.data[position].studentid
                                    setStringSharedPref(namesVariable.EDIT_STUDENT_ID,id,this@AddressListFragment)
//                                    toast(id)
                                }
                            }, this@AddressListFragment!!)
                            rv.adapter = categoryAdapters2
                            categoryAdapters2.notifyDataSetChanged()

//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun deleteAddress(deleteId: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = getStudent_Request()
            logReq.aid = deleteId
            val response = service.deleteAddress(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

//
//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                            getMyAddressesList()

//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }


    private fun deleteStudent(deleteId: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = getStudent_Request()
            logReq.studentid = deleteId

            val response = service.deleteStudents(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

//                            categoryItems2.clear()
//                            val result = response.body()
//
//                            for (x in result!!.data.indices){
//                                categoryItems2.add(result.data[x])
//                            }
//
//                            categoryAdapters2= getstudentAdapter(categoryItems2, object : getstudentAdapter.ItemClickListener{
//                                override fun clickToIntent(position: Int) {
//                                    deleteStudent()
//                                }
//                            }, this@AddressListFragment!!)
//                            rv.adapter = categoryAdapters2
//                            categoryAdapters2.notifyDataSetChanged()

//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
                            getStudentsList()

//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    private fun findViews() {
        addb1 = findViewById(R.id.add)
        addb2 = findViewById(R.id.add_address)
        nextButton = findViewById(R.id.addressproceed)

    }


    private fun initRecycler1() {
        rv=findViewById(R.id.addressList)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters= AddressAdapter(categoryItems, object : AddressAdapter.ItemClickListener{


            override fun clickToIntent(position: Int) {
            }

            override fun clickToIntent2(position: Int) {
                TODO("Not yet implemented")
            }

            override fun clickToIntent_delete(position: Int) {
                TODO("Not yet implemented")
            }


        }, this)
        rv.adapter = categoryAdapters
        categoryAdapters.notifyDataSetChanged()

    }


    private fun initRecycler2() {
        rv=findViewById(R.id.addressList)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        categoryAdapters2= getstudentAdapter(categoryItems2, object : getstudentAdapter.ItemClickListener{
            override fun clickToIntent(position: Int) {
            }

            override fun clickToIntent2(position: Int) {
                TODO("Not yet implemented")
            }

            override fun clickToIntent3(position: Int) {
                TODO("Not yet implemented")
            }

        }, this)
        rv.adapter = categoryAdapters2
        categoryAdapters2.notifyDataSetChanged()
    }


    private fun getMyAddressesList() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = getAddress_Request()
            logReq.userid = getStringSharedPref(namesVariable.USERID,this@AddressListFragment)

            val response = service.getAddress(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            categoryItems.clear()
                            val result = response.body()

                            for (x in result!!.data.indices){
                                categoryItems.add(result.data[x])
                            }

                            categoryAdapters= AddressAdapter(categoryItems, object : AddressAdapter.ItemClickListener{
                                override fun clickToIntent(position: Int) {
                                    setStringSharedPref(namesVariable.ADDRESS_ID,result.data[position].aid,this@AddressListFragment)
                                    val intent =  Intent (this@AddressListFragment, Edit_Address::class.java)
                                    intent.putExtra("FULLADDRESS", result.data[position].fulladdress)
                                    intent.putExtra("ADDRESS_LINE", result.data[position].address1)
                                    intent.putExtra("LOCALITY", result.data[position].locality)
                                    intent.putExtra("LAND", result.data[position].landmark)
                                    intent.putExtra("CITY", result.data[position].city)
                                    intent.putExtra("PINCODE", result.data[position].landmark)
                                    intent.putExtra("LAT", result.data[position].lat)
                                    intent.putExtra("LON", result.data[position].lon)

                                    startActivity(intent)

                                }

                                override fun clickToIntent2(position: Int) {

                                    val id = result.data[position].aid
                                    setStringSharedPref(namesVariable.ADDRESS_ID,id,this@AddressListFragment)
//                                    toast(id)
                                }

                                override fun clickToIntent_delete(position: Int) {
                                    val id = result.data[position].aid
                                    deleteAddress(id)
                                }


//                                override fun clickToIntent2(position: Int) {
//
//                                    val deleteId = result.data[position].aid
//                                    deleteMyAddress(deleteId)
//
//                                }
                            }, this@AddressListFragment!!)
                            rv.adapter = categoryAdapters
                            categoryAdapters.notifyDataSetChanged()




//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }



//    private fun deleteMyAddress(deleteId: String) {
//
//        CoroutineScope(Dispatchers.IO).launch {
//            val logReq = getStudent_Request()
//            logReq.studentid = deleteId
//
//            val response = service.deleteStudents(logReq)
//            try {
//                withContext(Dispatchers.Main) {
////                    loading.visibility = View.GONE
//                    if (response.body()!!.status == "true") {
//                        response.body()?.let {
//
////                            categoryItems2.clear()
////                            val result = response.body()
////
////                            for (x in result!!.data.indices){
////                                categoryItems2.add(result.data[x])
////                            }
////
////                            categoryAdapters2= getstudentAdapter(categoryItems2, object : getstudentAdapter.ItemClickListener{
////                                override fun clickToIntent(position: Int) {
////                                    deleteStudent()
////                                }
////                            }, this@AddressListFragment!!)
////                            rv.adapter = categoryAdapters2
////                            categoryAdapters2.notifyDataSetChanged()
//
//                            Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
//                            getStudentsList()
//
////
//                        }
//                    } else {
//
//                        Toast.makeText(this@AddressListFragment!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()
//
//                    }
//                }
//            } catch (e: HttpException) {
//                Log.e("REQUEST", "Exception ${e.message}")
//            } catch (e: Throwable) {
//                Log.e("REQUEST", "Ooops: Something else went wrong")
//            }
//        }
//
//
//
//    }


    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }




}
