package com.fabcoders.brunchbites.ui.choose_plan

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.fabcoders.brunchbites.R
import com.fabcoders.brunchbites.namesVariable
import com.fabcoders.brunchbites.setStringSharedPref
import com.fabcoders.brunchbites.toast
import com.fabcoders.brunchbites.ui.calendar.calendar


class choose_plan_days : AppCompatActivity() {
    lateinit var radio_group: RadioGroup
    lateinit var b1 : Button
    lateinit var l1: LinearLayout
    lateinit var l2: LinearLayout
    lateinit var l3: LinearLayout
    lateinit var l4: LinearLayout
    lateinit var l5: LinearLayout
    lateinit var radio:RadioButton
    var rvalue:String=""
    var  id: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_plan_days)
        setupToolbar()
        findViews()

        radio_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->

                radio = findViewById(checkedId)
                id = radio.id
                rvalue = radio.text.toString()
                setStringSharedPref(namesVariable.DAYS,rvalue,this)
//                    Toast.makeText(applicationContext," On checked change :"+ " ${radio.text}", Toast.LENGTH_SHORT).show()
            })

//        val newString: String?
//        newString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("STRING_CHOOSE_PLAN_DAYS")
//        } else {
//            savedInstanceState.getSerializable("STRING_CHOOSE_PLAN_DAYS") as String?
//        }
//
//        val thaalString: String?
//        thaalString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("THAAL_CHOOSE_PLAN_DAYS")
//        } else {
//            savedInstanceState.getSerializable("THAAL_CHOOSE_PLAN_DAYS") as String?
//        }
//        val eventString: String?
//        eventString = if (savedInstanceState == null) {
//            val extras = intent.extras
//            extras?.getString("EVENTS_CHOOSE_PLAN_DAYS")
//        } else {
//            savedInstanceState.getSerializable("EVENTS_CHOOSE_PLAN_DAYS") as String?
//        }


//        if (newString == "corporate"){
//            l1.setBackgroundResource(R.color.orange_4)
//            l2.setBackgroundResource(R.color.orange_2)
//            l3.setBackgroundResource(R.color.orange_6)
//            l4.setBackgroundResource(R.color.orange_7)
////            l5.setBackgroundResource(R.color.orange_7)
//        }else if(thaalString == "thaal"){
//            l1.setBackgroundResource(R.color.green6)
//            l2.setBackgroundResource(R.color.green3)
//            l3.setBackgroundResource(R.color.green6)
//            l4.setBackgroundResource(R.color.green1)
//        }else if(eventString == "events"){
//            l1.setBackgroundResource(R.color.purple1)
//            l2.setBackgroundResource(R.color.purple2)
//            l3.setBackgroundResource(R.color.purple5)
//            l4.setBackgroundResource(R.color.purple6)
//        }

        b1.setOnClickListener {

//            if(rvalue.equals("")) {
//                toast("Please select an option")
//            }else{
//                if (newString == "corporate") {
//                    val intent = Intent(this, calendar::class.java)
//                    val strName: String? = "corporate"
//                    val chday: String? = rvalue
//                    intent.putExtra("STRING_CHOOSE_CAL", strName)
//                    intent.putExtra("STRING_CHOOSE_CAL_DAY", chday)
//                    startActivity(intent)
//                } else if (thaalString == "thaal") {
//                    val intent = Intent(this, calendar::class.java)
//                    val strName2: String? = "thaal"
//                    val chday: String? = rvalue
//                    intent.putExtra("THAAL_CHOOSE_CAL", strName2)
//                    intent.putExtra("STRING_CHOOSE_CAL_DAY", chday)
//                    startActivity(intent)
//                } else if (eventString == "events") {
//                    val intent = Intent(this, calendar::class.java)
//                    val strName2: String? = "events"
//                    val chday: String? = rvalue
//                    intent.putExtra("EVENTS_CHOOSE_CAL", strName2)
//                    intent.putExtra("STRING_CHOOSE_CAL_DAY", chday)
//                    startActivity(intent)
//                } else {
//                    val intent = Intent(this, calendar::class.java)
//                    val chday: String? = rvalue
//                    intent.putExtra("STRING_CHOOSE_CAL_DAY", chday)
//                    startActivity(intent)
//                }
//
//            }
                if (!rvalue.equals("")){
                    startActivity(Intent(this, calendar::class.java))
                }else{
                    toast("Please select one Option")
                }





        }


    }

    private fun findViews() {
        radio_group = findViewById(R.id.radioGroup);
        b1 = findViewById(R.id.choose_planDays_next)
        l1 = findViewById(R.id.choose_plan_days_l1)
        l2 = findViewById(R.id.choose_plan_days_l2)
        l3 = findViewById(R.id.choose_plan_days_l3)
        l4 = findViewById(R.id.choose_plan_days_l4)

//        l5 = findViewById(R.id.choose_plan_l5) l1 = findViewById(R.id.choose_plan_days_l1)
//        l2 = findViewById(R.id.choose_plan_days_l2)
//        l3 = findViewById(R.id.choose_plan_days_l3)
//        l4 = findViewById(R.id.choose_plan_days_l4)
////        l5 = findViewById(R.id.choose_plan_l5)

    }
    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }


//    // Get the selected radio button text using radio button on click listener
//    fun radio_button_click(view: View){
//        // Get the clicked radio button instance
//        val radio: RadioButton = findViewById(radio_group.checkedRadioButtonId)
//        Toast.makeText(applicationContext,"On click : ${radio.text}",
//                Toast.LENGTH_SHORT).show()
//    }
}