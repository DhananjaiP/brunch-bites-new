package com.fabcoders.brunchbites.ui.Payment

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.model.model.cart.Cart_Request
import com.fabcoders.brunchbites.ui.myCart.myCart
import instamojo.library.InstamojoPay
import instamojo.library.InstapayListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.util.*

class Pay : AppCompatActivity() {
    lateinit var edtAmount:TextView
   lateinit var cmdPay:Button


    private fun callInstamojoPay(
        email: String,
        phone: String,
        amount: String,
        purpose: String,
        buyername: String
    ) {
        val activity: Activity = this
        val instamojoPay = InstamojoPay()
        val filter = IntentFilter("ai.devsupport.instamojo")
        registerReceiver(instamojoPay, filter)
        val pay = JSONObject()
        try {
            pay.put("email", email)
            pay.put("phone", phone)
            pay.put("purpose", purpose)
            pay.put("amount", amount)
            pay.put("name", buyername)
            pay.put("send_sms", true)
            pay.put("send_email", true)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        initListener()
        instamojoPay.start(activity, pay, listener)
    }

    var listener: InstapayListener? = null


    private fun initListener() {
        listener = object : InstapayListener {
            override fun onSuccess(response: String) {
//                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
//                        .show();
                Toast.makeText(applicationContext, "Payment Completed", Toast.LENGTH_SHORT).show()
                placeOrder()
                edtAmount.setText("")
                edtAmount.requestFocus()
            }

            override fun onFailure(code: Int, reason: String) {
                Toast.makeText(applicationContext, "Failed: $reason", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pay)

        // Call the function callInstamojo to start payment here
        edtAmount = findViewById(R.id.edtAmount)
        cmdPay = findViewById(R.id.cmdPay)

        edtAmount.setText( getStringSharedPref(namesVariable.GRAND_TOTAL,this))


        val email = getStringSharedPref(namesVariable.EMAIL,this)
        val phone = getStringSharedPref(namesVariable.PHONE_NUMBER,this)
        val name = getStringSharedPref(namesVariable.PERSON_NAME,this)


        cmdPay.setOnClickListener(View.OnClickListener {
            callInstamojoPay(email, phone, Objects.requireNonNull(edtAmount.getText()).toString(), "Testing", name)
        })
    }


    private fun placeOrder() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.customerid = getStringSharedPref(namesVariable.USERID,this@Pay)

            val response = service.ordernow(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()
                            toast(result!!.message)
                            startActivity(Intent(this@Pay, MainActivity::class.java))
//
                        }
                    } else {

                        Toast.makeText(this@Pay!!, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }
}