package com.fabcoders.brunchbites.ui.myCart

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.Adapter.cart_Extra_itemDetail_Adapter
import com.fabcoders.brunchbites.Adapter.cart_itemDetail_Adapter
import com.fabcoders.brunchbites.model.model.cart.Cart_Request
import com.fabcoders.brunchbites.model.model.cart.Date
import com.fabcoders.brunchbites.model.model.cart.Extraitem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.util.*
import kotlin.collections.ArrayList

class cart_item_detail : AppCompatActivity(){

    lateinit var itemAdapters: cart_itemDetail_Adapter
    lateinit var detailItems: ArrayList<Date>
    lateinit var rv: RecyclerView

    lateinit var ExtraitemAdapters: cart_Extra_itemDetail_Adapter
    lateinit var extradetailItems: ArrayList<Extraitem>
    lateinit var rv2: RecyclerView
    lateinit var lay: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alertdialog_mycart_details)
        setupToolbar()

        getViews()

        detailItems= java.util.ArrayList()

        extradetailItems= java.util.ArrayList()
        initRecycler1()
        initRecycler2()

        getdata()



    }

    private fun getViews() {
        lay = findViewById(R.id.extraitem_layout)
    }

    private fun initRecycler1() {
        rv=findViewById(R.id.recycler_dates)
        rv.layoutManager = LinearLayoutManager(this)
        rv.setHasFixedSize(true)
        itemAdapters= cart_itemDetail_Adapter(detailItems, object : cart_itemDetail_Adapter.ItemClickListener{
        }, this!!)
        rv.adapter = itemAdapters
        itemAdapters.notifyDataSetChanged()
    }

    private fun initRecycler2() {
        rv2=findViewById(R.id.recycler_extraitem)
        rv2.layoutManager = LinearLayoutManager(this)
        rv2.setHasFixedSize(true)
        ExtraitemAdapters= cart_Extra_itemDetail_Adapter(extradetailItems, object : cart_Extra_itemDetail_Adapter.ItemClickListener{
        }, this!!)
        rv2.adapter = ExtraitemAdapters
        ExtraitemAdapters.notifyDataSetChanged()
    }

    private fun getdata() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = Cart_Request()
            logReq.lineid = getStringSharedPref(namesVariable.LID,this@cart_item_detail)
            val response = service.cartItemDetail(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            detailItems.clear()
                            extradetailItems.clear()
                            val result = response.body()

                            for (x in result!!.dates.indices){
                                detailItems.add(result.dates[x])

                            }
                            detailItems.reverse()



                            itemAdapters= cart_itemDetail_Adapter(detailItems, object : cart_itemDetail_Adapter.ItemClickListener{

                            },this@cart_item_detail!!)
                            rv.adapter = itemAdapters
                            itemAdapters.notifyDataSetChanged()



                            if (result.extraitems.isEmpty()){
                                lay.visibility = View.GONE
                            }


                            for (x in result!!.extraitems.indices){
                                extradetailItems.add(result.extraitems[x])
                            }


                            ExtraitemAdapters= cart_Extra_itemDetail_Adapter(extradetailItems, object : cart_Extra_itemDetail_Adapter.ItemClickListener{

                            },this@cart_item_detail!!)
                            rv2.adapter = ExtraitemAdapters
                            ExtraitemAdapters.notifyDataSetChanged()



//                            Toast.makeText(this@cart_item_detail, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@cart_item_detail, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

}