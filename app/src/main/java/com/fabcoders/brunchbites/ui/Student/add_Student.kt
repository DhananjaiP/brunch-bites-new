package com.fabcoders.brunchbites.ui.Student

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.fabcoders.brunchbites.*
import com.fabcoders.brunchbites.model.model.Student.add_Student_Request
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class add_Student : AppCompatActivity() {

    lateinit var name :EditText
    lateinit var classd :EditText
    lateinit var submit :Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_edit_student)
        setupToolbar()
        name = findViewById(R.id.student_detail)
        classd = findViewById(R.id.class_detail)
        submit = findViewById(R.id.submit)








        submit.setOnClickListener {

            sendData()

        }
    }


    private fun sendData() {
        CoroutineScope(Dispatchers.IO).launch {
            val logReq = add_Student_Request()
            logReq.userid = getStringSharedPref(namesVariable.USERID,this@add_Student)
            logReq.studentname = name.text.toString()
            logReq.classdetail = classd.text.toString()
            val response = service.addStudent(logReq)
            try {
                withContext(Dispatchers.Main) {
//                    loading.visibility = View.GONE
                    if (response.body()!!.status == "true") {
                        response.body()?.let {

                            val result = response.body()

                            val studenId = result!!.studentid
                            setStringSharedPref(namesVariable.STUDENT_ID,studenId.toString(),this@add_Student)
                            setStringSharedPref(namesVariable.STUDENT_NAME,studenId.toString(),this@add_Student)
                            setStringSharedPref(namesVariable.STUDENT_CLASS,studenId.toString(),this@add_Student)



                            Toast.makeText(this@add_Student, "" + response.body()!!.message, Toast.LENGTH_LONG).show()


//
                        }
                    } else {

                        Toast.makeText(this@add_Student, "" + response.body()!!.message, Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbarText = findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text = getString(R.string.app_name)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_back_nice)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}