package com.fabcoders.brunchbites.ui.otp


import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.brunchbites.R
import kotlinx.android.synthetic.main.activity_verify_otp.*


class send_Otp : AppCompatActivity() {

    lateinit var button_send: Button
    lateinit var mobileNo_et :EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_otp)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_back_nice)
        findViews()
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        button_send.setOnClickListener {
            startActivity(Intent(this,verify_Otp::class.java))
        }
    }

    private fun findViews() {
        button_send = findViewById(R.id.forgetpass_send)
        mobileNo_et= findViewById(R.id.forgetpass_et)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}