package com.fabcoders.brunchbites

class Constants {
    companion object {
        const val CATEGORY_LIMIT: String = "6"
        const val CATEGORY_COLUMNS_COUNT: Int = 3
        const val VOLLEY_TIMEOUT: Int = 100000 //10secs
        const val VOLLEY_DEFAULT_MAX_RETRIES: Int = 20
        const val VOLLEY_DEFAULT_BACKOFF_MULT: Float = 1.0f
        const val PHONE_NUM_LENGTH: Int = 10
        const val PASSWORD_LENGTH: Int = 3

        //intent keys
        const val CATEGORY_ID: String = "CATEGORY_ID"
        const val CATEGORY_NAME: String = "CATEGORY_NAME"
        const val CATEGORY_IMAGE: String = "CATEGORY_IMAGE"
        const val KEYWORD: String = "KEYWORD"
        const val NAVIGATE_TO: String = "NAVIGATE_TO"
        const val FROM_SOCIAL: String = "FROM_SOCIAL"
        const val FROM_CHANGE: String = "FROM_CHANGE"
        const val DISCOUNTED_PRODUCTS: String = "DISCOUNTED_PRODUCTS"

        const val FROM_REGISTER: String = "FROM_REGISTER"
        const val PHONE: String = "PHONE"
        const val USER_ID: String = "USER_ID"
        const val ADDRESS: String = "ADDRESS"
        const val ADDRESS_ID: String = "ADDRESS_ID"
        const val CHECKOUT: String = "CHECKOUT"
        const val ORDER: String = "ORDER"
        const val OFFER: String = "OFFER"
        const val ORDER_ID: String = "ORDER_ID"
        const val PINCODE: String = "PINCODE"
        const val SLOT: String = "SLOT"
        const val DELIVERY_DATE: String = "DELIVERY_DATE"
        const val APPLIED_COUPON: String = "APPLIED_COUPON"
        const val SLOT_ID: String = "SLOT_ID"
        const val TIMESLOT: String = "TIMESLOT"
        const val CITY_PIN: String = "CITY_PIN"
        const val CITY_NAME: String = "CITY_NAME"
        const val GET_CITY: String = "GET_CITY"
        const val TITLE: String = "TITLE"
        const val URL: String = "URL"
        const val ORDER_NO: String = "ORDER_NO"
        const val DISCOUNT : String = "DISCOUNT"

        //NavigateTo types
        const val HOME: Int = 111
        const val CHANGE_PASSWORD: Int = 121

        //API response keys
        const val STATUS: String = "status"
        const val CODE: String = "code"

        //response codes
        const val BAD_REQUEST: Int = 400
        const val BAD_REQUEST_PHONE_PRESENT: Int = 401
        const val BAD_REQUEST_EMAIL_PRESENT: Int = 402
        const val MIN_AMOUNT: Int = 500
        const val SMS_SENDER: String = "FABCOD"

        //layouts
        const val ITEM_IMAGE_CART_SIZE = 76
        const val ITEM_FAB_SIZE = 36
        const val ITEM_BUTTON_WIDTH = 72
        const val ITEM_BUTTON_HEIGHT = 36
        const val ORDER_STEP: Int = 9
        const val HOME_SLIDER_SLIDE_DELAY: Long = 9000
    }
}