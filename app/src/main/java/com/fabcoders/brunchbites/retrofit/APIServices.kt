package com.fabcoders.maidan.retrofit



import com.fabcoders.brunchbites.Model.Address.getAddress_responce
import com.fabcoders.brunchbites.Model.Areas.area_responce
import com.fabcoders.brunchbites.Model.Categories.sub_category
import com.fabcoders.brunchbites.Model.Holiday.holiday_responce
import com.fabcoders.brunchbites.Model.Regions.region_responce
import com.fabcoders.brunchbites.Model.Registration.registeration_responce
import com.fabcoders.brunchbites.Model.Slider.slider_responce
import com.fabcoders.brunchbites.Model.calendar.getDates_responces
import com.fabcoders.brunchbites.Model.login.login_responce
import com.fabcoders.brunchbites.Model.schoool_List.school_list_Responce
import com.fabcoders.brunchbites.model.model.Address.Add_Edit_address_Request
import com.fabcoders.brunchbites.model.model.Address.add_edit_address_responce
import com.fabcoders.brunchbites.model.model.Address.edit_Responce
import com.fabcoders.brunchbites.model.model.Address.getAddress_Request
import com.fabcoders.brunchbites.model.model.Areas.areaRequest
import com.fabcoders.brunchbites.model.model.Categories.Category_responce
import com.fabcoders.brunchbites.model.model.Categories.categoriesRequest
import com.fabcoders.brunchbites.model.model.Categories.sub_categoriesRequest
import com.fabcoders.brunchbites.model.model.Holiday.holliday_Request
import com.fabcoders.brunchbites.model.model.Order.Order_Request
import com.fabcoders.brunchbites.model.model.Products.product_request
import com.fabcoders.brunchbites.model.model.Products.products_responce
import com.fabcoders.brunchbites.model.model.Regions.registration_Request
import com.fabcoders.brunchbites.model.model.Student.*
import com.fabcoders.brunchbites.model.model.calendar.getDates_Request
import com.fabcoders.brunchbites.model.model.cart.*
import com.fabcoders.brunchbites.model.model.getDaily.daily_Request
import com.fabcoders.brunchbites.model.model.getDaily.getDaily_responce
import com.fabcoders.brunchbites.model.model.login.loginRequest
import com.fabcoders.brunchbites.model.model.schoool_List.schoolList_Request
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface APIServices {




    @POST("user/login.php")
    suspend fun getLogin(@Body params: loginRequest): Response<login_responce>


    @POST("region/listregion.php")
    suspend fun getRegions(): Response<region_responce>

    @POST("user/create_user.php")
    suspend fun register(@Body params: registration_Request): Response<registeration_responce>

    @POST("products/get_categories.php")
    suspend fun getCategories(@Body params: categoriesRequest): Response<Category_responce>

    @POST("products/get_subcategories.php")
    suspend fun getSubCategories(@Body params: sub_categoriesRequest): Response<sub_category>

    @POST("products/get_sliders.php")
    suspend fun getSLiders(): Response<slider_responce>

    @POST("holidays/getholidays.php")
    suspend fun getHoliday(@Body params: holliday_Request): Response<holiday_responce>

    @POST("areas/listareas.php")
    suspend fun getAreas(@Body params: areaRequest): Response<area_responce>

    @POST("sb/listsb.php")
    suspend fun getSchoolAreas(@Body params: schoolList_Request): Response<school_list_Responce>

    @POST("user/getaddresses.php")
    suspend fun getAddress(@Body params: getAddress_Request): Response<getAddress_responce>

    @POST("user/getaddresses.php")
    suspend fun EditAddress(@Body params: Add_Edit_address_Request): Response<edit_Responce>

    @POST("products/getdates.php")
    suspend fun getDates(@Body params: getDates_Request): Response<getDates_responces>


    @POST("user/addaddress.php")
    suspend fun addEditAddress(@Body params: Add_Edit_address_Request): Response<add_edit_address_responce>

//    @POST("user/updatepassword.php")
//    suspend fun forgotpassword(@Body params: forgotpass_Request): Response<forgotpass_responce>


    @POST("products/get_duration.php")
    suspend fun chooseDailyorWeekly(@Body params: daily_Request): Response<getDaily_responce>


    @POST("products/getproductdetail.php")
    suspend fun getproducts(@Body params: product_request): Response<products_responce>


    @POST("user/getstudents.php")
    suspend fun getStudents(@Body params: getStudent_Request): Response<getStudent_responce>



    @POST("user/addstudent.php")
    suspend fun addStudent(@Body params: add_Student_Request): Response<add_edit_StudentResponce>

    @POST("user/deleteaddress.php")
    suspend fun deleteAddress(@Body params: getStudent_Request): Response<student_Delete_Responce>

    @POST("user/deletestudent.php")
    suspend fun deleteStudents(@Body params: getStudent_Request): Response<student_Delete_Responce>

    @POST("user/editstudent.php")
    suspend fun editStudents(@Body params: add_Student_Request): Response<student_Delete_Responce>

    @POST("products/getcustomerorders.php")
    suspend fun getOrdersList(@Body params: Order_Request): Response<orderList_Responce>

    @POST("products/getorderdetail.php")
    suspend fun getOrdersDetail(@Body params: Order_Request): Response<OrderDetail_responce>


    @POST("products/getcart.php")
    suspend fun getCart(@Body params: Cart_Request): Response<getCart_Responce>

    @POST("products/removelinefromcart.php")
    suspend fun Delete_cart_Item(@Body params: Cart_Request): Response<student_Delete_Responce>

    @POST("products/applyvoucher.php")
    suspend fun applyCoupon(@Body params: Cart_Request): Response<student_Delete_Responce>

    @POST("products/clearvoucher.php")
    suspend fun removCoupon(@Body params: Cart_Request): Response<student_Delete_Responce>


    @POST("products/addproducttocart.php")
    suspend fun addProductsToCart(@Body params: JsonObject): Response<add_to_cart_cal_responce>

    @POST("products/addextraproductstocart.php")
    suspend fun addExtraItemsToCart(@Body params: JsonObject): Response<student_Delete_Responce>



    @POST("products/addstudenttocart.php")
    suspend fun addStudentToCart(@Body params: Add_student_tocart_Request): Response<edit_Responce>

    @POST("products/addaddresstocart.php")
    suspend fun addAddressToCart(@Body params: Cart_Request): Response<edit_Responce>

    @POST("products/getcartlinedetail.php")
    suspend fun cartItemDetail(@Body params: Cart_Request): Response<cart_ItemDetails>


    @POST("products/convertcarttoorder.php")
    suspend fun ordernow(@Body params: Cart_Request): Response<edit_Responce>

    @POST("user/forgotpassword.php")
    suspend fun forgotpass(@Body params: loginRequest): Response<edit_Responce>


}

