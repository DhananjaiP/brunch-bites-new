package com.fabcoders.brunchbites

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.fabcoders.brunchbites.Model.Slider.Data


@Dao
interface BannerDao {

    @Query("SELECT * from offers ORDER BY sliderid")
    fun getAll(): List<Data>

    @Insert(onConflict = REPLACE)
    fun insert(bannerModel: Data)

    @Insert(onConflict = REPLACE)
    fun insertAll(bannerModel: List<Data>)

    @Query("DELETE from offers")
    fun deleteAll()
}